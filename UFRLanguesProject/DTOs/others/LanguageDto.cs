﻿using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs.others
{
    public class LanguageDto
    {
        public LanguageEnum Type { get; set; }
    }
}
