﻿using DTOs.exercise;
using DTOs.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs.group
{
    public class GroupDto
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public GroupDto OverGroup { get; set; }
    }
}
