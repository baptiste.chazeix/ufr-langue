﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs.user
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public List<RoleDto> Roles { get; set; } = new List<RoleDto>(); 
    }
}
