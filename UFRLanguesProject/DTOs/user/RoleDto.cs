﻿using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs.user
{
    public class RoleDto
    {
        public int Id { get; set; }
        public RoleEnum Type { get; set; }
    }
}
