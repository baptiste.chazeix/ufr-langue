﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs.exercise
{
    public class QuestionDto
    {
        public int Id { get; set; }
        public string Sentence { get; set; }
        public List<CorrectionDto> Corrections { get; set; } = new List<CorrectionDto>();
    }
}
