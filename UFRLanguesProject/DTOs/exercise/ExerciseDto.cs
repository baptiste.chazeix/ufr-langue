﻿using DTOs.others;
using System;
using System.Collections.Generic;

namespace DTOs.exercise
{
    public class ExerciseDto
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public List<QuestionDto> Questions { get; set; } = new List<QuestionDto>();
        public LanguageDto Language { get; set; }
    }
}
