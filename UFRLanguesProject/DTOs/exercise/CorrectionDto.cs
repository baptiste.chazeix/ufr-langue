﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs.exercise
{
    public class CorrectionDto
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string Answer { get; set; }
    }
}
