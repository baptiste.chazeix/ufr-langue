﻿using System;
using System.Collections.Generic;

namespace Model.exercise
{
    public interface ICorrection
    {
        Dictionary<int, string> Correct();
        bool IsQuestionCorrect();
    }
}
