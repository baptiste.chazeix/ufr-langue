﻿using Model.exception;
using System;
using System.Collections.Generic;

namespace Model.exercise
{
    public class Exercise
    {
        public int Id { get; set; }

        public List<Question> Questions { get; set; } = new List<Question>();

        public string Name { get; set; }

        public Exercise(String name)
        {
            this.Name = name;
        }

        public Exercise()
        {

        }

        public void AddQuestion(Question question)
        {
            if(!Questions.Contains(question))
            {
                Questions.Add(question);
            }
        }
        
        public void RemoveQuestion(Question question)
        {
            if (Questions.Contains(question))
            {
                Questions.Remove(question);
            }
            else { throw new NotContainsException("This question does not exist in this exercise context");  }
        }

        public void RemoveQuestion(int index)
        {
            if(index < 0 || index < Questions.Count)
            {
                Questions.RemoveAt(index);
            }
            else { throw new IndexOutOfRangeException(); }
        }
    }
}
