﻿using Model.exception;
using System;
using System.Collections.Generic;

namespace Model.exercise
{
    public abstract class Question : ICorrection
    {
        public int Id { get; set; }

        public string Sentence { get; set; }

        // Dictionnary which contains => key: index to answer; value: answer
        public Dictionary<int, string> Answers = new Dictionary<int, string>();

        public Question()
        {
        }

        // cut a string in an array of words
        public string[] CutSentence()
        {
            return Sentence.Split();
        }

        // add index i in the array index
        protected void AddIndex(int i, string answer)
        {
            if (i >= 0 && i < CutSentence().Length)
            {
                Answers[i] = answer;
            }
            else { throw new IndexOutOfRangeException(); }

        }
        // remove index i of the array index 
        public void RemoveIndex(int i)
        {
            Answers.Remove(i);
        }

        // return true or false
        // Allows to know if the answer is correct
        public bool IsAnswerCorrect(int ind, string answer)
        {
            if (Answers.ContainsKey(ind))
            {
                if (Answers[ind].Equals(answer))
                {
                    RemoveIndex(ind);
                    return true;
                }
                return false;
            }
            throw new NotIndexToAnswerException("This index does not need to be corrected");
        }

        public Dictionary<int, string> Correct()
        {
            return Answers;
        }

        public bool IsQuestionCorrect()
        {
            return Answers.Count == 0;
        }
    }
}
