﻿using Model.exception;
using System;
using System.Collections.Generic;

namespace Model.exercise
{
    public class GaspText : Question
    {

        //Constructor
        public GaspText()
        {
        }

        public void AddIndex(int i) 
        {
            AddIndex(i, CutSentence()[i]);        
        }

        // return the sentence with the missing words
        public string[] GetSentenceToComplete()
        {
            string[] sent = CutSentence();

            foreach(int ind in Answers.Keys)
            {
                sent[ind] = "_____";
            }

            return sent;
        }
    }
}
