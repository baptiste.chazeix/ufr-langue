﻿using System;
using System.Collections.Generic;

namespace Model.exercise
{
    public class Colouring : Question
    {

        public Colouring()
        {
        }

        public new void AddIndex(int i, string answer)
        {
            base.AddIndex(i,answer);
        }

        public int NumberOfWordsRemaining()
        {
            return Answers.Count;
        }

        public string[] GetGrammaticalFunctionsToFind()
        {
            List<string> functionsRemaining = new List<string>();
            foreach (string value in Answers.Values)
            {
                functionsRemaining.Add(value);
            }
            return functionsRemaining.ToArray();
        }
    }
}
