﻿using System;
using System.Collections.Generic;
using Model.user;

namespace Model.group
{
    public class Group : MasterGroup
    {
        private List<User> students;

        public Group(int ID) : base(ID)
        {
            students = new List<User>();
        }

        public Group(List<User> students, int ID): base(ID)
        {
            this.students = students;
        }

        public bool AddStudent(User student)
        {
            if (!students.Contains(student))
            {
                students.Add(student);
                return true;
            }
            else
            {
                return false;
            }
        }

        public IReadOnlyList<User> GetStudents()
        {
            return students.AsReadOnly();
        }

        public override IReadOnlyList<User> GetStudents(int ID)
        {
            if (base.ID == ID)
            {
                return students.AsReadOnly();
            }
            else
            {
                throw new Exception("ID erroné");
            }
        }

        public bool RemoveStudent(User student)
        {
            if (students.Contains(student))
            {
                students.Remove(student);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
