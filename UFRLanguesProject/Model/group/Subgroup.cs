﻿using System;
using System.Collections.Generic;
using Model.user;

namespace Model.group
{
    public class Subgroup : MasterGroup
    {
        private List<MasterGroup> groups;

        public Subgroup(int ID) : base(ID)
        {
            groups = new List<MasterGroup>();
        }

        public Subgroup(List<MasterGroup> masterGroups, int ID) : base(ID)
        {
            groups = masterGroups;
        }

        public bool AddSuperGroup(MasterGroup superGroup)
        {
            if (!groups.Contains(superGroup))
            {
                groups.Add(superGroup);
                return true;
            }
            return false;
            
        }

        public override IReadOnlyList<User> GetStudents(int ID)
        {
            foreach(MasterGroup group in groups)
            {
                if (group.ID == ID)
                {
                    return ((Group)group).GetStudents();
                }
            }
            throw new Exception("ID non trouvé");
        }

        public bool RemoveSuperGroup(MasterGroup superGroup)
        {
            if(groups.Contains(superGroup))
            {
                groups.Remove(superGroup);
                return true;
            }
            return false;
        }
    }
}
