﻿using System;
using System.Collections.Generic;
using Model.user;

namespace Model.group
{
    public abstract class MasterGroup
    {
        public readonly int ID;

        public MasterGroup(int ID)
        {
            this.ID = ID;
        }

        public abstract IReadOnlyList<User> GetStudents(int ID);
    }
}
