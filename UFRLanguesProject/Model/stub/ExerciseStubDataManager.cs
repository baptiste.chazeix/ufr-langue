﻿using System;
using Shared;
using Model.exercise;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace Model.stub
{
    public class ExerciseStubDataManager : IDataManager<Exercise>
    {

        List<Exercise> stubEx;
        public ExerciseStubDataManager()
        {
            stubEx = new List<Exercise>()
            {
                new Exercise("lala1"),
                new Exercise("lala2")
            };
        }

        public async Task<bool> AddRange(params Exercise[] items)
        {
            throw new NotImplementedException();
        }

        public Task Clear()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public Task<Exercise> FindById(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Exercise>> GetItems(int index, int count)
        {

            return await Task.FromResult(stubEx.Skip(index * count).Take(count));
        }

        public Task<Exercise> Insert(Exercise item)
        {
            throw new NotImplementedException();
        }

        public Task<Exercise> Update(object id, Exercise item)
        {
            throw new NotImplementedException();
        }

    }
}
