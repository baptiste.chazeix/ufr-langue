﻿using System;
using Model.exercise;
using Model.group;
using Model.user;

namespace Model.stub
{
    public class Stub
    {

        private Loader loader { get; set; }

        public Stub()
        {
        }



        public Group CreateGroupStudentsWithModerator(int id)
        {
            Group studentsGroup = new Group(id);
            studentsGroup.AddStudent(CreateStudent("Claude", "Etudiant", new DateTime(2000, 05, 07)));
            studentsGroup.AddStudent(CreateStudent("jean-Marc", "Etudiant", new DateTime(2001, 05, 07)));
            studentsGroup.AddStudent(CreateStudentModerator("Marc-Claude", "Moderateur", new DateTime(1952, 04, 16)));
            return studentsGroup;
        }

        public Group CreateGroupStudents(int id)
        {
            Group studentsGroup = new Group(id);
            studentsGroup.AddStudent(CreateStudent("jean", "Etudiant", new DateTime(2000, 05, 07)));
            studentsGroup.AddStudent(CreateStudent("jean-Claude", "Etudiant", new DateTime(2001, 05, 07)));
            studentsGroup.AddStudent(CreateStudent("jean-Claude", "Duss", new DateTime(1952, 04, 16)));
            return studentsGroup;
        }

        public User CreateTeacher(string name, string surname, DateTime birthday)
        {
            User teacherUser = new User { FirstName = name, LastName = surname, BirthDate = birthday };
            teacherUser.AddRole(new Teacher());
            return teacherUser;
        }

        public User CreateStudent(string name, string surname, DateTime birthday)
        {
            User studentUser = new User { FirstName = name, LastName = surname, BirthDate = birthday };
            studentUser.AddRole(new Student());
            return studentUser;
        }

        public User CreateStudentModerator(string name, string surname, DateTime birthday)
        {
            User studentUser = new User { FirstName = name, LastName = surname, BirthDate = birthday };
            studentUser.AddRole(new Student());
            studentUser.AddRole(new Moderator());
            return studentUser;
        }

        public User CreateModerator(string name, string surname, DateTime birthday)
        {
            User studentUser = new User { FirstName = name, LastName = surname, BirthDate = birthday };
            studentUser.AddRole(new Moderator());
            return studentUser;
        }

        public Exercise CreateGaspExercice()
        {
            Exercise gaspEx = new Exercise("gaspExo");
            gaspEx.AddQuestion(new GaspText { Sentence = "zero un deux trois quatre" });
            gaspEx.AddQuestion(new GaspText { Sentence = "le mot a completer est : mot" });
            return gaspEx;
        }


        public Exercise CreateColouringExercice()
        {
            Exercise ColourEx = new Exercise("ColorExo");
            ColourEx.AddQuestion(new GaspText { Sentence = "je suis une phrase" });
            ColourEx.AddQuestion(new GaspText { Sentence = "je suis un projet informatique" });
            return ColourEx;
        }

    }
}
