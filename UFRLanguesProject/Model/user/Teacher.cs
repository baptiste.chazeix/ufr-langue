﻿using System;
using System.Collections.Generic;
using Model.exercise;
using Model.group;

namespace Model.user
{
    public class Teacher : Moderator, IPublish
    {
        public static readonly string TEACHER = "TEACHER";

        public Teacher() : base()
        {
        }

        // La publication de l'exercice publiera la correction par extention
        public bool PublishExercise(Exercise exercise)
        {
            if (exercise != null)
            {
                // Attention a qui a créer l'exercice car pour l'instant on ne vérifie que dans sa liste
                if (MyExercises.Contains(exercise))
                {
                    // Connnection avec la BDD pour publier l'exercice en param

                    /// if exercice n'ait pas déjà publié ///
                        return true;
                    /// fin si ///
                    //else
                    //{
                    //    throw new Exception("Exercice already publish");
                    //}
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public bool CreateGroup()
        {
            throw new NotImplementedException();
        }

        public override string GetRole()
        {
            return TEACHER;
        }
    }
}
