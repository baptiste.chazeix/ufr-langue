﻿using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Model.exercise;

namespace Model.user
{
    public abstract class Role 
    {
        public int Id { get; set; }
        public RoleEnum Type { get; set; }

        protected Role()
        {
        }

        public List<Exercise> MyExercises { get; set; } = new List<Exercise>();

        public void AddExercises(Exercise exercise)
        {
            MyExercises.Add(exercise);
        }

        public abstract string GetRole();

        public override string ToString()
        {
            return new StringBuilder(GetRole()).Append("\n").ToString();
        }
    }
}
