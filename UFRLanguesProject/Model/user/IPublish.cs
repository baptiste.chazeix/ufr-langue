﻿using System;
using Model.exercise;

namespace Model.user
{

    public interface IPublish
    {
       bool PublishExercise(Exercise exercise);
    }

    
}
