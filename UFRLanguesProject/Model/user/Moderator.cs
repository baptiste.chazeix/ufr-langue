﻿using System;
using System.Collections.Generic;
using Model.exercise;
using Model.group;

namespace Model.user
{
    public class Moderator : Role, IModerate
    {
        public static readonly string MODERATOR = "MODERATOR";

        public MasterGroup MasterGroup { get; protected set; }
        

        public Moderator()
        {
        }

        public bool GroupChange(MasterGroup newMasterGroup)
        {
            if (newMasterGroup != null)
            {
                MasterGroup = newMasterGroup;
                return true;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public bool CreateExercice(Exercise exercise)
        {
            if (exercise != null)
            {
                MyExercises.ForEach(exo =>
               {
                   if (exo.Equals(exercise))
                   {
                       throw new ArgumentException("Exercise already exist");
                   }
               });
                MyExercises.Add(exercise);
                // Envoyer sur la database sans publier
                return true;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public bool RemoveExercise(int index)
        {
            if (index < MyExercises.Count)
            {
                MyExercises.RemoveAt(index);
                return true;
            }
            else
            {
                throw new IndexOutOfRangeException();
            }
        }

        public bool RemoveExercise(Exercise exercise)
        {
            if (exercise != null)
            {
                MyExercises.Remove(exercise);
                return true;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public override string GetRole()
        {
            return MODERATOR;
        }
    }
}
