﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.connection;
using Model.exception;

namespace Model.user
{
    public class User
    {
        public string FirstName;
        public string LastName;
        public DateTime BirthDate;
        protected List<Language> Languages = new List<Language>();
        public List<Role> Roles = new List<Role>();
        protected Uca Connection;

        public User()
        {
        }

        public bool AddRole(Role role)
        {
            if (role != null)
            {
                try
                {
                    CheckRole(role.GetRole());
                    return false;
                }
                catch
                {
                    Roles.Add(role);
                    return true;
                }
            }
            throw new ArgumentNullException();
        }


        /********** A compléter comme au dessus ************/
        //public bool AddRole(List<Role> roleList)
        //{
        //    if (roleList != null)
        //    {
        //        roleList.ForEach(role =>
        //        {
        //            roles.Add(role);
        //        });
        //        return true;
        //    }
        //    throw new ArgumentNullException();
        //}
        /***************************************************/

        public bool RemoveRole(Role role)
        {
            if (role != null)
            {
                Roles.Remove(role);
                return true;
            }
            throw new ArgumentNullException();
        }

        public bool RemoveRole(int index)
        {
            if (index < Roles.Count)
            {
                Roles.RemoveAt(index);
                return true;
            }
            throw new IndexOutOfRangeException();
        }

        /**
         *
         * Fonction retournant le rôle recherché ou null si l'utilisateur ne le possède pas
         * 
         */
        public Role CheckRole(string role)
        {
            foreach (Role r in Roles)
            {
                if (role == r.GetRole())
                {
                    return r;
                }
            }
            throw new RoleNotFoundException("Rôle non trouvé !");
        }

        public int GetAge()
        {
            return DateTime.Now.Year - BirthDate.Year -
                    (DateTime.Now.Month < BirthDate.Month ? 1 :
                    (DateTime.Now.Month == BirthDate.Month && DateTime.Now.Day < BirthDate.Day) ? 1 : 0);
        }

        public bool NewLanguageSubscription(Language lang)
        {
            if (!CheckLangGroup(lang))
            {
                Languages.Add(lang);
                return true;
            }
            return false;
        }

        public bool ForgetLanguageSubscription(Language lang)
        {
            if (CheckLangGroup(lang))
            {
                Languages.Remove(lang);
                return true;
            }
            return false;
        }

        public bool CheckLangGroup(Language lang)
        {
            foreach (Language l in Languages)
            {
                if (lang.Equals(l))
                {
                    return true;
                }
            }
            return false;
        }

        public string GetName()
        {
            return new StringBuilder(FirstName).Append(" ").Append(LastName).ToString();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder("Je suis ").Append(FirstName).Append(" ").Append(LastName).Append(" et j'ai ").Append(GetAge().ToString()).Append(" ans !\n Par ailleurs, je suis :\n");
            Roles.ForEach(role =>
            {
                stringBuilder.Append(role);
            });
            return stringBuilder.ToString();
        }
    }
}
