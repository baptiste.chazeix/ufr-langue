﻿using System;
using System.Collections.Generic;
using Model.exercise;

namespace Model.user
{
    public class Student : Role
    {
        public static readonly string STUDENT = "STUDENT";

        public Student()
        {
        }

        /**
         *
         * Fonction permettant d'envoyer une réponse pour un exercice
         *
         */
        public bool DoAnExercice(Exercise exercise)
        {
            if (exercise != null)
            {
                if (!MyExercises.Contains(exercise))
                {
                    // Contact database to publish his answer to exercise
                    MyExercises.Add(exercise);
                    return true;
                }
                else
                {
                    throw new Exception("Exercise already done");
                }
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public void DownloadExerciceCorrection(Exercise exercise)
        {
            // Connect on database to get exercise's correction
            throw new NotImplementedException();
        }

        public override string GetRole()
        {
            return STUDENT;
        }

        // Méthode checkStatus
    }
}
