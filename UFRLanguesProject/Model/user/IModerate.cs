﻿using System;
using Model.exercise;
using Model.group;

namespace Model.user
{
    public interface IModerate
    {
        bool GroupChange(MasterGroup newMasterGroup);

        bool CreateExercice(Exercise exercise);

        bool RemoveExercise(int index);

        bool RemoveExercise(Exercise exercise);
    }
}
