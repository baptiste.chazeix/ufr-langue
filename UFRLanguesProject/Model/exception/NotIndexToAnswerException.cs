﻿using System;
using System.Runtime.Serialization;

namespace Model.exception
{
    [Serializable]
    public class NotIndexToAnswerException : Exception
    {
        public NotIndexToAnswerException()
        {
        }

        public NotIndexToAnswerException(string message) : base(message)
        {
        }

        public NotIndexToAnswerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotIndexToAnswerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
