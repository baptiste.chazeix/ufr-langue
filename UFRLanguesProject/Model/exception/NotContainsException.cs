﻿using System;
using System.Runtime.Serialization;

namespace Model.exception
{
    [Serializable]
    public class NotContainsException : Exception
    {
        public NotContainsException()
        {
        }

        public NotContainsException(string message) : base(message)
        {
        }

        public NotContainsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotContainsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}