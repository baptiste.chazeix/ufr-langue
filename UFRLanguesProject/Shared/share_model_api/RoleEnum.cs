﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Shared.share_model_api
{
    public enum RoleEnum
    {
        STUDENT,
        MODERATOR,
        TEACHER
    }
}
