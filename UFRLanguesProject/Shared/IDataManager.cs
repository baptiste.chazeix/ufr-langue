﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared
{
    public interface IDataManager<T> where T : class
    {
        Task<T> FindById(object id);

        Task<IEnumerable<T>> GetItems(int index, int count);

        Task<T> Insert(T item);

        Task<bool> AddRange(params T[] items);

        Task<T> Update(object id, T item);

        Task<bool> Delete(object id);

        Task Clear();
    }
}
