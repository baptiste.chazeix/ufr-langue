﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exotic.view
{
    public partial class GroupSelectorPage : ContentPage
    {
        public GroupSelectorPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void GroupSelector_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ExercisesPage());
        }
    }
}