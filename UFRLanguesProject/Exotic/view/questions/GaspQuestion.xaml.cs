﻿using Model.exercise;
using Model.stub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exotic.view.questions
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GaspQuestion : ContentPage
    {
        GaspText Exercise;
        string Sentence;
        public GaspQuestion()
        {
            Stub stub = new Stub();
            Exercise = stub.CreateGaspExercice().Questions[0] as GaspText;
            Sentence = Exercise.Sentence;

            BindingContext = this;
            InitializeComponent();
        }
    }
}