﻿using Model.exercise;
using ModelApiLink;
using Shared;
using SharedMVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace Exotic.view
{
    public partial class ExercisesPage : ContentPage
    {
        public ObservableCollection<ExerciseVM> Exercises { get; set; }

        public IDataManager<Exercise> DataManager => (App.Current as App).DataManager;

        public ExercisesPage()
        {
            Exercises = new ObservableCollection<ExerciseVM>();

            StartLoadingExercises();

            BindingContext = this;

            InitializeComponent();
        }

        private async void StartLoadingExercises()
        {
            IEnumerable<Exercise> exercises = await DataManager.GetItems(0, 5);
            exercises.ForEach(ex => {
                Exercises.Add(new ExerciseVM(ex));
            });
        }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListView list = (ListView)sender;
            ExerciseVM vm = (ExerciseVM)list.SelectedItem;
            await Navigation.PushAsync(new ExercisePage(vm));
        }
    }
}