﻿using Model.exercise;
using SharedMVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exotic.view
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExercisePage : ContentPage
    {
        public ObservableCollection<Question> Questions { get; set; }
        public string ExerciseTitle { get; set; }



        public ExercisePage(ExerciseVM vm)
        {
            InitializeComponent();

            ExerciseTitle = vm.Name;

            Questions = new ObservableCollection<Question>(vm.Questions);

            BindingContext = this;
        }
    }
}