﻿using Exotic.view;
using Exotic.view.questions;
using Model.exercise;
using ModelApiLink;
using Shared;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exotic
{
    public partial class App : Application
    {
        public IDataManager<Exercise> DataManager { get; set; } = new StubExerciseDataManager();
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
