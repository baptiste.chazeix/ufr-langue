﻿using System.Threading.Tasks;
using DataLibrary;
using DataLibrary.exercise;
using DataLibrary.group;
using DataLibrary.user;
using Microsoft.EntityFrameworkCore;
using static System.Console;

namespace testDataLibrary
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //Dans la console gestionnaire de package nuget
            //dotnet tool install --global dotnet-ef --version 3.0.0
            //Si dans data library
            //dotnet ef migrations add Migrations --startup-project ..\testDataLibrary
            //dotnet ef database update --startup-project ..\testDataLibrary
            //Si dans le projet de lancement
            //dotnet ef migrations add Migrations --project ..\DataLibrary
            //dotnet ef database update --project ..\DataLibrary

            using (var dbContext = new TableStub())
            using (var unitOfWork = new UnitOfWork(dbContext))
            {
                /////////////////////////////////////////
                WriteLine("///// USER SECTION /////");
                /////////////////////////////////////////

                var users = unitOfWork.Repository<UserEntity>().GetItems(0, 30).Result;
                foreach (var n in users)
                {
                    WriteLine($"User : {n.Id}, Firstname : {n.FirstName}, Lastname : {n.LastName}, Birthdate : {n.BirthDate}");
                    WriteLine("Languages :");
                    foreach(var language in n.Languages)
                    {
                        WriteLine($"--> {language.Type}");
                    }
                    foreach (var role in n.Roles)
                    {
                        WriteLine($"Role : {role.Id}");
                    }
                    WriteLine();
                }

                WriteLine();

                /////////////////////////////////////////
                WriteLine("///// EXERCISE SECTION /////");
                /////////////////////////////////////////

                var exercises = unitOfWork.Repository<ExerciseEntity>().GetItems(0, 30).Result;
                foreach (var ex in exercises)
                {
                    WriteLine($"Exercise : {ex.Id} -> Language = {ex.Language.Type}");
                    WriteLine("Categories :");
                    foreach(var category in ex.Categories)
                    {
                        WriteLine($"-> {category.Name} > {category.Description}");
                    }
                    foreach (var question in ex.Questions)
                    {
                        WriteLine($"Question {question.Id} : {question.Sentence}");
                        foreach (var correction in question.Corrections)
                        {
                            WriteLine($"Correction : The answer of the index {correction.Index} is => {correction.Answer}");
                        }
                    }
                    WriteLine();
                }

                WriteLine();

                /////////////////////////////////////////
                WriteLine("///// GROUP SECTION /////");
                /////////////////////////////////////////

                var groups = unitOfWork.Repository<GroupEntity>().GetItems(0, 30).Result;
                foreach (var group in groups)
                {
                    WriteLine($"Group {group.Id} : {group.GroupName}");
                    WriteLine("Users :");
                    foreach (var user in group.Users)
                    {
                        WriteLine($"> {user.FirstName} {user.LastName}");
                    }
                    WriteLine("Exercises :");
                    foreach (var exercise in group.Exercises)
                    {
                        WriteLine($"> Id = {exercise.Id}");
                    }
                    WriteLine("Subgroups :");
                    foreach (var sg in group.SubGroups)
                    {
                        WriteLine($"> {sg.GroupName}");
                    }
                    WriteLine();
                }
            }
        }
    }
}
