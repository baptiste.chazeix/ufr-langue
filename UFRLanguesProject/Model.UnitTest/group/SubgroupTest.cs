﻿using System;
using Model.group;
using Xunit;

namespace Model.UnitTest.group
{
    public class SubgroupTest
    {

        private Subgroup subgroup = new Subgroup(6);
        private Group group = new Group(7);


        [Fact]
        public void TestAddSuperGroup()
        {
            Assert.True(subgroup.AddSuperGroup(group));

            Assert.False(subgroup.AddSuperGroup(group));
        }


        [Fact]
        public void TestGetStudents()
        {
            Assert.Throws<Exception>(() => subgroup.GetStudents(99));
        }


        [Fact]
        public void TestRemoveSuperGroup()
        {
            Assert.False(subgroup.RemoveSuperGroup(group));

            Assert.True(subgroup.AddSuperGroup(group));

            Assert.True(subgroup.RemoveSuperGroup(group));
        }

    }
}
