﻿using System;
using Xunit;
using Model.group;
using Model.user;
using System.Collections.Generic;

namespace Model.UnitTest.group
{
    public class GroupTest
    {

        private Group group = new Group(5);
        private User student = new User { FirstName = "Pierre", LastName = "Paul", BirthDate = new DateTime(785, 05, 24) };


        [Fact]
        public void TestAddStudent()
        {
            Assert.True(group.AddStudent(student));

            Assert.False(group.AddStudent(student));
        }


        [Fact]
        public void TestGetStudents()
        {
            Assert.Throws<Exception>(() => group.GetStudents(99));
        }


        [Fact]
        public void TestRemoveStudent()
        {
            Assert.False(group.RemoveStudent(student));

            Assert.True(group.AddStudent(student));

            Assert.True(group.RemoveStudent(student));
        }

    }
}
