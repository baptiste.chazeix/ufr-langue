﻿using Model.exercise;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Model.UnitTest.exercise
{
    public class ColouringTest
    {

        private Colouring CreateAColouringInstance(string sentence)
        {
            return new Colouring { Sentence = sentence };
        }

        [Fact]
        public void TestCutSentence()
        {
            Colouring cl = CreateAColouringInstance("just a sample");
            string[] splitSentence = cl.CutSentence();
            Assert.Equal(3, splitSentence.Length);
        }

        [Fact]
        public void TestAddIndex()
        {
            Colouring cl = CreateAColouringInstance("sentence is complete");
            cl.AddIndex(1,"verb");
            Assert.Single(cl.Answers);
            // must throw an exception because there are less than 3 words in the sentence
            Assert.Throws<IndexOutOfRangeException>(() => cl.AddIndex(3,"nothing"));
        }

        [Fact]
        public void TestRemoveIndex()
        {
            Colouring cl = CreateAColouringInstance("sentence to test");
            cl.AddIndex(0,"subject");
            cl.RemoveIndex(1);
            Assert.Single(cl.Answers);

            cl.RemoveIndex(0);
            Assert.Empty(cl.Answers);
        }

        [Fact]
        public void TestIsAnswerCorrect()
        {
            Colouring cl = CreateAColouringInstance("what is this word");
            cl.AddIndex(3,"name");
            Assert.False(cl.IsAnswerCorrect(3, "sentence"));

            Assert.Single(cl.Answers);
            Assert.True(cl.IsAnswerCorrect(3, "name"));
            Assert.Empty(cl.Answers);
        }

        [Fact]
        public void TestCorrect()
        {
            Colouring cl = CreateAColouringInstance("sentence to be corrected");
            cl.AddIndex(2,"verb");
            Dictionary<int, string> dicCorrect = cl.Correct();
            Assert.Single(dicCorrect);
            Assert.Contains("verb", dicCorrect[2]);

            cl.RemoveIndex(2);
            dicCorrect = cl.Correct();
            Assert.Empty(dicCorrect);
        }

        [Fact]
        public void TestNumberOfWordsRemaining()
        {
            Colouring cl = CreateAColouringInstance("sentence to be answer");
            cl.AddIndex(0,"subject");
            cl.AddIndex(2, "verb");
            Assert.Equal(2, cl.NumberOfWordsRemaining());

            cl.RemoveIndex(0);
            Assert.Equal(1, cl.NumberOfWordsRemaining());
        }

        [Fact]
        public void TestGetGrammaticalFunctionsToFind()
        {
            Colouring cl = CreateAColouringInstance("sentence to be answer");
            cl.AddIndex(0, "subject");
            cl.AddIndex(2, "verb");

            string[] functionsToFind = cl.GetGrammaticalFunctionsToFind();
            Assert.Equal(2, functionsToFind.Length);
            Assert.Equal("subject", functionsToFind[0]);

            cl.RemoveIndex(0);
            functionsToFind = cl.GetGrammaticalFunctionsToFind();
            Assert.Single(functionsToFind);
            Assert.Equal("verb", functionsToFind[0]);
        }
    }
}
