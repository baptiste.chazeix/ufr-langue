﻿using Model.exercise;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Model.UnitTest.exercise
{
    public class GaspTextTest
    {

        private GaspText CreateAGaspTextInstance(string sentence)
        {
            return new GaspText { Sentence = sentence };
        }

        [Fact]
        public void TestCutSentence()
        {
            GaspText gt = CreateAGaspTextInstance("just a sample");
            string[] splitSentence = gt.CutSentence();
            Assert.Equal(3, splitSentence.Length);
        }

        [Fact]
        public void TestAddIndex()
        {
            GaspText gt = CreateAGaspTextInstance("sentence to complete");
            gt.AddIndex(1);
            Assert.Single(gt.Answers);
            // must throw an exception because there are less than 3 words in the sentence
            Assert.Throws<IndexOutOfRangeException>(() => gt.AddIndex(3));
        }

        [Fact]
        public void TestRemoveIndex()
        {
            GaspText gt = CreateAGaspTextInstance("sentence to test");
            gt.AddIndex(0);
            gt.RemoveIndex(1);
            Assert.Single(gt.Answers);

            gt.RemoveIndex(0);
            Assert.Empty(gt.Answers);
        }

        [Fact]
        public void TestIsAnswerCorrect()
        {
            GaspText gt = CreateAGaspTextInstance("what is this word");
            gt.AddIndex(3);
            Assert.False(gt.IsAnswerCorrect(3, "sentence"));

            Assert.Single(gt.Answers);
            Assert.True(gt.IsAnswerCorrect(3, "word"));
            Assert.Empty(gt.Answers);
        }

        [Fact]
        public void TestGetSentenceToComplete()
        {
            GaspText gt = CreateAGaspTextInstance("sentence to complete");
            gt.AddIndex(1);
            string[] sentenceToComplete = gt.GetSentenceToComplete();
            Assert.Contains("_", sentenceToComplete[1]);

            gt.RemoveIndex(1);
            sentenceToComplete = gt.GetSentenceToComplete();
            Assert.DoesNotContain("_", sentenceToComplete[1]);
        }

        [Fact]
        public void TestCorrect()
        {
            GaspText gt = CreateAGaspTextInstance("sentence to be corrected");
            gt.AddIndex(3);
            Dictionary<int, string> dicCorrect = gt.Correct();
            Assert.Single(dicCorrect);
            Assert.Contains("corrected", dicCorrect[3]);

            gt.RemoveIndex(3);
            dicCorrect = gt.Correct();
            Assert.Empty(dicCorrect);
        }
    }
}
