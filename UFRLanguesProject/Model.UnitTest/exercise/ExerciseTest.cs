using Model.exception;
using Model.exercise;
using System;
using Xunit;

namespace Model.UnitTest
{
    public class ExerciseTest
    {
        private Exercise CreateAnExerciseWithOneSentence()
        {
            Exercise exercise = new Exercise("exo");
            exercise.AddQuestion(new GaspText { Sentence ="test sentence" });
            return exercise;
        }

        [Fact]
        public void TestAddQuestion()
        {
            Assert.Single(CreateAnExerciseWithOneSentence().Questions);
        }

        [Fact]
        public void TestRemoveQuestion()
        {
            Exercise exercise = CreateAnExerciseWithOneSentence();
            exercise.RemoveQuestion(0);

            Assert.Empty(exercise.Questions);

            // method must throw an exception because the list is now empty
            Assert.Throws<IndexOutOfRangeException>(() => exercise.RemoveQuestion(0));
            //method must throw an exception because this question is not in the list
            Assert.Throws<NotContainsException>(() => exercise.RemoveQuestion(new Colouring { Sentence = "test sentence" }));
        }
    }
}
