﻿using System;
using Model.exercise;
using Model.user;
using Xunit;

namespace Model.UnitTest.user
{
    public class StudentTest
    {
        Student studentRole = new Student();

        private Exercise exo = new Exercise("exo");

        [Fact]
        public void TestDoAnExercice()
        {
            Assert.Throws<ArgumentNullException>( () => studentRole.DoAnExercice(null));

            Assert.True(studentRole.DoAnExercice(exo));

            Assert.Throws<Exception>(() => studentRole.DoAnExercice(exo));
        }


        [Fact]
        public void TestDowloadExerciceCorrection()
        {
            Assert.Throws<NotImplementedException>(() => studentRole.DownloadExerciceCorrection(exo));
        }
    }
}
