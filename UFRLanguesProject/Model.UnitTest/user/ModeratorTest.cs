﻿using System;
using Model.exercise;
using Model.group;
using Model.user;
using Xunit;

namespace Model.UnitTest.user
{
    public class ModeratorTest
    {

        private Moderator moderator = new Moderator();
        protected Exercise exo = new Exercise("exo");


        [Fact]
        public void TestGroupChange()
        {
            Assert.Throws<ArgumentNullException>(() => moderator.GroupChange(null));

            Assert.True(moderator.GroupChange(new Group(2)));
        }


        [Fact]
        public void TestCreateExercice()
        {
            Assert.Throws<ArgumentNullException>(() => moderator.CreateExercice(null));

            Assert.True(moderator.CreateExercice(exo));

            Assert.Throws<ArgumentException>(() => moderator.CreateExercice(exo));
        }


        [Fact]
        public void TestRemoveExercise()
        {
            Assert.True(moderator.CreateExercice(exo));

            Assert.Throws<IndexOutOfRangeException>(() => moderator.RemoveExercise(10));

            Assert.True(moderator.RemoveExercise(0));

            Assert.Throws<ArgumentNullException>(() => moderator.RemoveExercise(null));

            Assert.True(moderator.RemoveExercise(exo));
        }
    }
}
