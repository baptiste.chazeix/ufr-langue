using System;
using Xunit;
using Model.user;
using Model.group;
using System.Collections.Generic;
using Model.exception;

namespace Model.UnitTest.user
{
    public class UserTest
    {

        private User user = new User {
            FirstName ="prenom",
            LastName = "nom",
            BirthDate = new DateTime(1998, 12, 06)
        };
        private Role teacherRole = new Teacher();


        [Fact]
        public void TestAddRole()
        {
            Assert.Throws<ArgumentNullException>(() => user.AddRole(null));

            Assert.True(user.AddRole(teacherRole));

            Assert.False(user.AddRole(teacherRole));
        }


        [Fact]
        public void TestRemoveRole()
        {
            Assert.True(user.AddRole(teacherRole));

            Assert.Throws<IndexOutOfRangeException>(() => user.RemoveRole(10));

            Assert.True(user.RemoveRole(0));

            Assert.Throws<ArgumentNullException>(() => user.RemoveRole(null));

            Assert.True(user.RemoveRole(teacherRole));
        }


        [Fact]
        public void TestCheckRole()
        {
            Assert.Throws<RoleNotFoundException>(() => user.CheckRole("role"));

            Assert.Throws<RoleNotFoundException>(() => user.CheckRole(Teacher.TEACHER));

            Assert.True(user.AddRole(teacherRole));

            Assert.Equal(teacherRole, user.CheckRole(Teacher.TEACHER));

            Assert.Throws<RoleNotFoundException>(() => user.CheckRole(Moderator.MODERATOR));
        }


        [Fact]
        public void TestNewLanguageSubscription()
        {
              
            Assert.True(user.NewLanguageSubscription(Language.Grec));

            Assert.False(user.NewLanguageSubscription(Language.Grec));
        }


        [Fact]
        public void TestForgetLanguageSubscription()
        {

            Assert.False(user.ForgetLanguageSubscription(Language.Grec));
            Assert.True(user.NewLanguageSubscription(Language.Grec));
            Assert.True(user.ForgetLanguageSubscription(Language.Grec));
            
        }


        [Fact]
        public void TestCheckLangGroup()
        {
            Assert.True(user.NewLanguageSubscription(Language.Grec));

            Assert.False(user.CheckLangGroup(Language.Latin));

            Assert.True(user.CheckLangGroup(Language.Grec));
        }
    }
}
