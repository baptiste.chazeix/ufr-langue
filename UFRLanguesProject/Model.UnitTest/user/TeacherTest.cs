﻿using System;
using Model.group;
using Model.user;
using Xunit;

namespace Model.UnitTest.user
{
    public class TeacherTest : ModeratorTest
    {
        private Teacher teacher = new Teacher();


        [Fact]
        public void TestPublishExercise()
        {
            Assert.Throws<ArgumentNullException>(() => teacher.PublishExercise(null));

            Assert.Throws<ArgumentOutOfRangeException>(() => teacher.PublishExercise(exo));

            Assert.True(teacher.CreateExercice(exo));

            Assert.True(teacher.PublishExercise(exo));
        }

        [Fact]
        public void TestCreateGroup()
        {
            Assert.Throws<NotImplementedException>(() => teacher.CreateGroup());
        }
    }
}
