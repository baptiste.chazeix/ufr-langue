﻿using DataLibrary.group;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DataLibrary.Tests
{
    public class GroupLibrary_Tests
    {
        /*
        private async Task<GroupEntity[]> CreateSomeGroups(UnitOfWork unitOfWork)
        {
            GroupEntity firstGroup = new GroupEntity { Id = 4, GroupName = "New_Group" };
            firstGroup.SubGroups.Add(await unitOfWork.Repository<GroupEntity>().FindById(1));

            GroupEntity secondGroup = new GroupEntity { Id = 5, GroupName = "2A_G1" };

            GroupEntity thirdGroup = new GroupEntity { Id = 6, GroupName = "2A" };
            thirdGroup.SubGroups.Add(secondGroup);

            return new GroupEntity[] { firstGroup, secondGroup, thirdGroup };
        }

        [Fact]
        public async void FindByIdTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var firstGroup = await unitOfWork.Repository<GroupEntity>().FindById(1);
                var secondGroup = await unitOfWork.Repository<GroupEntity>().FindById(2);
                var notCreatedGroup = await unitOfWork.Repository<GroupEntity>().FindById(7);

                // Not null
                Assert.NotNull(firstGroup);
                Assert.NotNull(secondGroup);

                // Null
                Assert.Null(notCreatedGroup);

                // Number of subgroups
                Assert.Equal(2, firstGroup.SubGroups.Count());
                Assert.Empty(secondGroup.SubGroups);

                // Group verifications
                Assert.Equal("MasterGroup", firstGroup.GroupName);
                Assert.Equal("MasterGroup_G1", secondGroup.GroupName);

                // Subgroups verifications
                Assert.Equal(2, firstGroup.SubGroups.ElementAt(0).Id);
                Assert.Equal(3, firstGroup.SubGroups.ElementAt(1).Id);
            }
        }


        [Fact]
        public async void GetItemsTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                Assert.Equal(3, (await unitOfWork.Repository<GroupEntity>().GetItems(0, 10)).Count());
                Assert.Single(await unitOfWork.Repository<GroupEntity>().GetItems(2, 10));
                Assert.Empty(await unitOfWork.Repository<GroupEntity>().GetItems(3, 10));
            }
        }


        [Fact]
        public async void InsertTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Creating a new group
                GroupEntity newGroup = new GroupEntity { Id = 4, GroupName = "New_Group" };
                newGroup.SubGroups.Add(await unitOfWork.Repository<GroupEntity>().FindById(1));

                await unitOfWork.Repository<GroupEntity>().Insert(newGroup);

                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                Assert.Equal(4, (await unitOfWork.Repository<GroupEntity>().GetItems(0, 10)).Count());
                var insertedGroup = await unitOfWork.Repository<GroupEntity>().FindById(newGroup.Id);
                Assert.NotNull(insertedGroup);

                Assert.Equal("New_Group", insertedGroup.GroupName);
                Assert.Single(insertedGroup.SubGroups);
                Assert.Equal(1, insertedGroup.SubGroups.ElementAt(0).Id);
            }
        }


        [Fact]
        public async void AddRangeTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var groupsToAdd = await CreateSomeGroups(unitOfWork);

                // AddRange
                await unitOfWork.Repository<GroupEntity>().AddRange(groupsToAdd);

                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                Assert.Equal(6, (await unitOfWork.Repository<GroupEntity>().GetItems(0, 10)).Count());

                var firstInsertedGroup = await unitOfWork.Repository<GroupEntity>().FindById(groupsToAdd.ElementAt(0).Id);
                Assert.NotNull(firstInsertedGroup);
                var thirdInsertedGroup = await unitOfWork.Repository<GroupEntity>().FindById(groupsToAdd.ElementAt(2).Id);
                Assert.NotNull(thirdInsertedGroup);

                Assert.Single(firstInsertedGroup.SubGroups);
                Assert.Single(thirdInsertedGroup.SubGroups);

                // Check the first group values
                Assert.Equal("New_Group", firstInsertedGroup.GroupName);
                Assert.Equal(1, firstInsertedGroup.SubGroups.ElementAt(0).Id);
                // Retrieving the whole group associated
                var subgroupAssociated = await unitOfWork.Repository<GroupEntity>().FindById(firstInsertedGroup.SubGroups.ElementAt(0).Id);
                Assert.Equal("MasterGroup", subgroupAssociated.GroupName);

                // Check the last group values
                Assert.Equal("2A", thirdInsertedGroup.GroupName);
                Assert.Equal(5, thirdInsertedGroup.SubGroups.ElementAt(0).Id);
                // Retrieving the whole group associated
                subgroupAssociated = await unitOfWork.Repository<GroupEntity>().FindById(thirdInsertedGroup.SubGroups.ElementAt(0).Id);
                Assert.Equal("2A_G1", subgroupAssociated.GroupName);
            }
        }


        [Fact]
        public async void UpdateTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var groupToUpdate = await unitOfWork.Repository<GroupEntity>().FindById(2);
                groupToUpdate.GroupName = "Undefined_Group";

                // Update
                await unitOfWork.Repository<GroupEntity>().Update(groupToUpdate);
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                var updatedGroup = await unitOfWork.Repository<GroupEntity>().FindById(2);
                Assert.NotNull(updatedGroup);

                Assert.Equal("Undefined_Group", updatedGroup.GroupName);
            }
        }


        [Fact]
        public async void DeleteTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Delete
                await unitOfWork.Repository<GroupEntity>().Delete(1);
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                Assert.Equal(2, (await unitOfWork.Repository<GroupEntity>().GetItems(0, 10)).Count());
                Assert.Null(await unitOfWork.Repository<GroupEntity>().FindById(1));
            }
        }


        [Fact]
        public async void ClearTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Clear
                await unitOfWork.Repository<GroupEntity>().Clear();
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                Assert.Empty(await unitOfWork.Repository<GroupEntity>().GetItems(0, 10));
            }
        }


        [Fact]
        public async void RejectChangesTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Clear
                await unitOfWork.Repository<GroupEntity>().Clear();
                // Reject Changes
                await unitOfWork.RejectChangesAsync();
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Make sure nothing has been deleted
                Assert.Equal(3, (await unitOfWork.Repository<GroupEntity>().GetItems(0, 10)).Count());
            }
        }
        */
    }
}
