﻿using DataLibrary.user;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DataLibrary.Tests
{
    public class UserLibrary_Tests
    {
        /*
        private async Task<UserEntity[]> CreateSomeUsers(UnitOfWork unitOfWork)
        {
            var roleRepository = unitOfWork.Repository<RoleEntity>() as RoleRepository;
            var moderatorRole = await roleRepository.GetOrCreateById(RoleEnum.MODERATOR);
            var teacherRole = await roleRepository.GetOrCreateById(RoleEnum.TEACHER);
            var studentRole = await roleRepository.GetOrCreateById(RoleEnum.STUDENT);

            UserEntity firstUser = new UserEntity { Id = 3, FirstName = "Laurent", LastName = "Perrot", BirthDate = new DateTime(1996, 6, 23) };
            firstUser.AddRole(moderatorRole);

            UserEntity secondUser = new UserEntity { Id = 4, FirstName = "Catherine", LastName = "Lebrun", BirthDate = new DateTime(1972, 3, 1) };
            secondUser.AddRole(teacherRole);

            UserEntity thirdUser = new UserEntity { Id = 5, FirstName = "Fabrice", LastName = "Saindon", BirthDate = new DateTime(1995, 1, 31) };
            thirdUser.AddRole(studentRole);
            thirdUser.AddRole(moderatorRole);

            return new UserEntity[] { firstUser, secondUser, thirdUser };
        }

        [Fact]
        public async void FindByIdTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var firstUser = await unitOfWork.Repository<UserEntity>().FindById(1);
                var secondUser = await unitOfWork.Repository<UserEntity>().FindById(2);
                var notCreatedUser = await unitOfWork.Repository<UserEntity>().FindById(7);

                // Not null
                Assert.NotNull(firstUser);
                Assert.NotNull(secondUser);

                // Null
                Assert.Null(notCreatedUser);

                // Number of roles
                Assert.Equal(2, firstUser.Roles.Count());
                Assert.Single(secondUser.Roles);

                // User verifications
                Assert.Equal("Marc", firstUser.FirstName);
                Assert.Equal("Moreau", secondUser.LastName);
                Assert.Equal(2000, firstUser.BirthDate.Year);
                Assert.Equal(26, secondUser.BirthDate.Day);

                // Roles verifications
                Assert.Equal(RoleEnum.STUDENT, firstUser.Roles.ElementAt(0).Id);
                Assert.Equal(RoleEnum.TEACHER, secondUser.Roles.ElementAt(0).Id);
            }
        }


        [Fact]
        public async void GetItemsTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                Assert.Equal(2, (await unitOfWork.Repository<UserEntity>().GetItems(0, 10)).Count());
                Assert.Single(await unitOfWork.Repository<UserEntity>().GetItems(1, 10));
                Assert.Empty(await unitOfWork.Repository<UserEntity>().GetItems(2, 10));
            }
        }


        [Fact]
        public async void InsertTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Creating a new user
                UserEntity newUser = new UserEntity { Id = 3, FirstName = "Laurent", LastName = "Perrot", BirthDate = new DateTime(1996, 6, 23) };
                newUser.AddRole(
                    new RoleEntity { Id = RoleEnum.MODERATOR }
                );

                await unitOfWork.Repository<UserEntity>().Insert(newUser);

                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                Assert.Equal(3, (await unitOfWork.Repository<UserEntity>().GetItems(0, 10)).Count());
                var insertedUser = await unitOfWork.Repository<UserEntity>().FindById(newUser.Id);
                Assert.NotNull(insertedUser);

                Assert.Equal("Laurent", insertedUser.FirstName);
                Assert.Equal(1996, insertedUser.BirthDate.Year);
                Assert.Single(insertedUser.Roles);
                Assert.Equal(RoleEnum.MODERATOR, insertedUser.Roles.ElementAt(0).Id);
            }
        }


        [Fact]
        public async void AddRangeTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var usersToAdd = await CreateSomeUsers(unitOfWork);

                // AddRange
                await unitOfWork.Repository<UserEntity>().AddRange(usersToAdd);

                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                Assert.Equal(5, (await unitOfWork.Repository<UserEntity>().GetItems(0, 10)).Count());

                var firstInsertedUser = await unitOfWork.Repository<UserEntity>().FindById(usersToAdd.ElementAt(0).Id);
                Assert.NotNull(firstInsertedUser);
                var thirdInsertedUser = await unitOfWork.Repository<UserEntity>().FindById(usersToAdd.ElementAt(2).Id);
                Assert.NotNull(thirdInsertedUser);

                Assert.Single(firstInsertedUser.Roles);
                Assert.Equal(2, thirdInsertedUser.Roles.Count());

                // Check the first user values
                Assert.Equal("Laurent", firstInsertedUser.FirstName);
                Assert.Equal(6, firstInsertedUser.BirthDate.Month);
                Assert.Equal(RoleEnum.MODERATOR, firstInsertedUser.Roles.ElementAt(0).Id);

                // Check the last user values
                Assert.Equal("Fabrice", thirdInsertedUser.FirstName);
                Assert.Equal(1995, thirdInsertedUser.BirthDate.Year);
                Assert.Equal(RoleEnum.STUDENT, thirdInsertedUser.Roles.ElementAt(0).Id);
            }
        }


        [Fact]
        public async void UpdateTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var userToUpdate = await unitOfWork.Repository<UserEntity>().FindById(2);
                userToUpdate.FirstName = "Jean";
                userToUpdate.BirthDate = new DateTime(1997, 3, 14);

                // Update
                await unitOfWork.Repository<UserEntity>().Update(userToUpdate);
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                var updatedUser = await unitOfWork.Repository<UserEntity>().FindById(2);
                Assert.NotNull(updatedUser);

                Assert.Equal("Jean", updatedUser.FirstName);
                Assert.Equal("Moreau", updatedUser.LastName);
                Assert.Equal(14, updatedUser.BirthDate.Day);
            }
        }


        [Fact]
        public async void DeleteTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Delete
                await unitOfWork.Repository<UserEntity>().Delete(1);
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                Assert.Single(await unitOfWork.Repository<UserEntity>().GetItems(0, 10));
                Assert.Null(await unitOfWork.Repository<UserEntity>().FindById(1));
            }
        }


        [Fact]
        public async void ClearTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Clear
                await unitOfWork.Repository<UserEntity>().Clear();
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                Assert.Empty(await unitOfWork.Repository<UserEntity>().GetItems(0, 10));
            }
        }


        [Fact]
        public async void RejectChangesTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Clear
                await unitOfWork.Repository<UserEntity>().Clear();
                // Reject Changes
                await unitOfWork.RejectChangesAsync();
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Make sure nothing has been deleted
                Assert.Equal(2, (await unitOfWork.Repository<UserEntity>().GetItems(0, 10)).Count());
            }
        }
        */
    }
}
