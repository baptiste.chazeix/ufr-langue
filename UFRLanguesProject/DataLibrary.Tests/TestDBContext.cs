﻿using System;
using System.Collections.Generic;
using DataLibrary.user;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace DataLibrary.Tests
{
    public class TestDBContext : TableStub
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            optionBuilder.UseSqlite(connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
