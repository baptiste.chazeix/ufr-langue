﻿using DataLibrary.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace DataLibrary.Tests
{
    public class ExerciseLibrary_Tests
    {
        /*
        private ExerciseEntity[] CreateSomeExercises()
        {
            ExerciseEntity firstExercise = new ExerciseEntity { Id = 3 };
            firstExercise.Questions.Add(
                new QuestionEntity { Id = 4, Sentence = "Cette question teste la méthode AddRange" }
            );
            firstExercise.Questions.ElementAt(0).Corrections.Add(
                new CorrectionEntity { Id = 5, Index = 5, Answer = "AddRange" }
            );

            ExerciseEntity secondExercise = new ExerciseEntity { Id = 4 };
            secondExercise.Questions.Add(
                new QuestionEntity { Id = 5, Sentence = "Cette question teste aussi la méthode AddRange" }
            );
            secondExercise.Questions.ElementAt(0).Corrections.Add(
                new CorrectionEntity { Id = 6, Index = 1, Answer = "question" }
            );

            ExerciseEntity thirdExercise = new ExerciseEntity { Id = 5 };
            thirdExercise.Questions.Add(
                new QuestionEntity { Id = 6, Sentence = "Cette question est inutile" }

            );
            thirdExercise.Questions.ElementAt(0).Corrections.Add(
                new CorrectionEntity { Id = 7, Index = 3, Answer = "inutile" }
            );

            return new ExerciseEntity[] { firstExercise, secondExercise, thirdExercise };
        }

        [Fact]
        public async void FindByIdTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var firstExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(1);
                var secondExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(2);
                var notCreatedExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(7);

                // Not null
                Assert.NotNull(firstExercise);
                Assert.NotNull(secondExercise);

                // Null
                Assert.Null(notCreatedExercise);

                // Number of questions 
                Assert.Single(firstExercise.Questions);
                Assert.Equal(2, secondExercise.Questions.Count);

                // Sentence verification
                var firstQuestion = firstExercise.Questions.ElementAt(0);
                Assert.Equal("Ceci est la premiere question", firstQuestion.Sentence);

                // Correction
                Assert.Equal(2, firstQuestion.Corrections.Count);
                Assert.Equal(3, firstQuestion.Corrections.ElementAt(1).Index);
                Assert.Equal("premiere", firstQuestion.Corrections.ElementAt(1).Answer);
            }
        }

        
        [Fact]
        public async void GetItemsTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                Assert.Equal(2, (await unitOfWork.Repository<ExerciseEntity>().GetItems(0, 10)).Count());
                Assert.Single(await unitOfWork.Repository<ExerciseEntity>().GetItems(1, 10));
                Assert.Empty(await unitOfWork.Repository<ExerciseEntity>().GetItems(2, 10));
            }
        }

        
        [Fact]
        public async void InsertTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Creating a new exercise
                ExerciseEntity newExercise = new ExerciseEntity { Id = 3 };
                newExercise.Questions.Add(
                    new QuestionEntity { Id = 4, Sentence = "Cette question teste la méthode Insert" }
                );
                newExercise.Questions.ElementAt(0).Corrections.Add(
                    new CorrectionEntity { Id = 5, Index = 5, Answer = "Insert" }
                );

                await unitOfWork.Repository<ExerciseEntity>().Insert(newExercise);

                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                Assert.Equal(3, (await unitOfWork.Repository<ExerciseEntity>().GetItems(0, 10)).Count());
                var insertedExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(newExercise.Id);
                Assert.NotNull(insertedExercise);

                Assert.Single(insertedExercise.Questions);
                Assert.Single(insertedExercise.Questions.ElementAt(0).Corrections);
                Assert.Equal(5, insertedExercise.Questions.ElementAt(0).Corrections.ElementAt(0).Index);
                Assert.Equal("Insert", insertedExercise.Questions.ElementAt(0).Corrections.ElementAt(0).Answer);
            }
        }


        [Fact]
        public async void AddRangeTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var exercisesToAdd = CreateSomeExercises();

                // AddRange
                await unitOfWork.Repository<ExerciseEntity>().AddRange(exercisesToAdd);

                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                Assert.Equal(5, (await unitOfWork.Repository<ExerciseEntity>().GetItems(0, 10)).Count());

                var firstInsertedExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(exercisesToAdd.ElementAt(0).Id);
                Assert.NotNull(firstInsertedExercise);
                var thirdInsertedExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(exercisesToAdd.ElementAt(2).Id);
                Assert.NotNull(thirdInsertedExercise);

                Assert.Single(firstInsertedExercise.Questions);
                Assert.Single(firstInsertedExercise.Questions.ElementAt(0).Corrections);

                // Check the first exercise corrections values
                Assert.Equal(5, firstInsertedExercise.Questions.ElementAt(0).Corrections.ElementAt(0).Index);
                Assert.Equal("AddRange", firstInsertedExercise.Questions.ElementAt(0).Corrections.ElementAt(0).Answer);

                // Check the last exercise questions values
                Assert.Equal(6, thirdInsertedExercise.Questions.ElementAt(0).Id);
                Assert.Equal("Cette question est inutile", thirdInsertedExercise.Questions.ElementAt(0).Sentence);
            }
        }


        [Fact]
        public async void UpdateTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                var exerciseToUpdate = await unitOfWork.Repository<ExerciseEntity>().FindById(2);
                exerciseToUpdate.Questions.ElementAt(0).Sentence = "Je modifie la question";
                exerciseToUpdate.Questions.ElementAt(0).Corrections.ElementAt(0).Index = 1;
                exerciseToUpdate.Questions.ElementAt(0).Corrections.ElementAt(0).Answer = "modifie";

                // Update
                await unitOfWork.Repository<ExerciseEntity>().Update(exerciseToUpdate);
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Check the results
                var updatedExercise = await unitOfWork.Repository<ExerciseEntity>().FindById(2);
                Assert.NotNull(updatedExercise);

                Assert.Equal(2, updatedExercise.Questions.Count);
                Assert.Equal("Je modifie la question", updatedExercise.Questions.ElementAt(0).Sentence);
                Assert.Single(updatedExercise.Questions.ElementAt(0).Corrections);
                Assert.Equal(1, updatedExercise.Questions.ElementAt(0).Corrections.ElementAt(0).Index);
                Assert.Equal("modifie", updatedExercise.Questions.ElementAt(0).Corrections.ElementAt(0).Answer);
            }
        }


        [Fact]
        public async void DeleteTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Delete
                await unitOfWork.Repository<ExerciseEntity>().Delete(1);
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                Assert.Single(await unitOfWork.Repository<ExerciseEntity>().GetItems(0, 10));
                Assert.Null(await unitOfWork.Repository<ExerciseEntity>().FindById(1));
            }
        }


        [Fact]
        public async void ClearTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Clear
                await unitOfWork.Repository<ExerciseEntity>().Clear();
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                Assert.Empty(await unitOfWork.Repository<ExerciseEntity>().GetItems(0, 10));
            }
        }


        [Fact]
        public async void RejectChangesTest()
        {
            using (var context = new TestDBContext())
            using (var unitOfWork = new UnitOfWork(context))
            {
                context.Database.EnsureCreated();

                // Clear
                await unitOfWork.Repository<ExerciseEntity>().Clear();
                // Reject Changes
                await unitOfWork.RejectChangesAsync();
                // Saving the modifications
                await unitOfWork.SaveChangesAsync();

                // Make sure nothing has been deleted
                Assert.Equal(2, (await unitOfWork.Repository<ExerciseEntity>().GetItems(0, 10)).Count());
            }
        }
        */
    }
}
