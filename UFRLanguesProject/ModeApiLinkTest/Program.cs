﻿using Model.exercise;
using ModelApiLink;
using System;
using System.Threading.Tasks;

namespace ModeApiLinkTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ExerciseDataManager dataManager = new ExerciseDataManager();

            // Get All
            var exercises = await dataManager.GetItems(0, 10);

            //Find by ID
            var firstEx = await dataManager.FindById(1);

            // Insert
            /*
            var exerciseToUpdate = new Exercise { Name = "FirstExercise" };
            exerciseToUpdate.Questions.Add(new GaspText { Sentence = "First Question" });

            var exerciseCreated = await dataManager.Insert(exerciseToUpdate);

            exerciseToUpdate.Questions.Clear();


            var exerciseUpdated = await dataManager.Update(exerciseCreated.Id, exerciseToUpdate);
            */

            QuestionDataManager qDataManager = new QuestionDataManager(1);

            await qDataManager.Insert(new GaspText { Sentence = "Ceci est une nouvelle question" });

            await qDataManager.Delete(1);

            await qDataManager.Update(11, new GaspText { Sentence = "Rechangement de la question" });

            Console.WriteLine("debug");
        }
    }
}
