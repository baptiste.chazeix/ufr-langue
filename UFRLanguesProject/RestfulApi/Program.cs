using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NLog.Web;

namespace RestfulApi
{
    public class Program
    {
        /*
         * Démarrage de la restful : Voir [https://gitlab.iut-clermont.uca.fr/bachazeix/ufr-langue/blob/master/README.md]
         */

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseNLog();
                    webBuilder.UseStartup<Startup>();
                });
    }
}
