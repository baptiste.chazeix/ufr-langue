﻿using DataLibrary.exercise;
using DataLibrary.others;
using DTOs.exercise;
using DTOs.others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Profile
{
    public class ExerciseProfile : AutoMapper.Profile
    {
        public ExerciseProfile()
        {
            CreateMap<ExerciseDto, ExerciseEntity>().ReverseMap();
            CreateMap<QuestionDto, QuestionEntity>().ReverseMap();
            CreateMap<CorrectionDto, CorrectionEntity>().ReverseMap();
            CreateMap<LanguageDto, LanguageEntity>().ReverseMap();
        }
    }
}
