﻿using DataLibrary.group;
using DTOs.group;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Profile
{
    public class GroupProfile : AutoMapper.Profile
    {
        public GroupProfile()
        {
            CreateMap<GroupDto, GroupEntity>().ReverseMap();
        }
    }
}
