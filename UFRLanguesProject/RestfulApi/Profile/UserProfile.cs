﻿using DataLibrary.user;
using DTOs.user;

namespace RestfulApi.Profile
{
    public class UserProfile : AutoMapper.Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, UserEntity>().ReverseMap();
            CreateMap<RoleDto, RoleEntity>().ReverseMap();
        }
    }
}
