﻿using AutoMapper;
using DataLibrary;
using DataLibrary.exercise;
using DataLibrary.many_to_many;
using DataLibrary.user;
using DTOs.user;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly ILogger<RoleController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RoleController(ILogger<RoleController> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<RoleDto>))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll([FromQuery] int index = 0, [FromQuery] int count = 10)
        {
            _logger.LogDebug("call getAll method");

            var roles = await _unitOfWork.Repository<RoleEntity>().GetItems(index, count);
            var rolesDto = _mapper.Map<IEnumerable<RoleDto>>(roles);

            return Ok(rolesDto);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(RoleDto))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            _logger.LogDebug("call get method");
            var role = await _unitOfWork.Repository<RoleEntity>().FindById(id);
            if (role != null)
            {
                var roleDto = _mapper.Map<RoleDto>(role);
                return Ok(roleDto);
            }
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(RoleDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]RoleDto role)
        {
            _logger.LogDebug("call post method");

            if (role == null)
            {
                _logger.LogError("error invalid post request");
                return BadRequest();
            }

            var roleEntity = _mapper.Map<RoleEntity>(role);
            await _unitOfWork.Repository<RoleEntity>().Insert(roleEntity);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = role.Id }, role);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(RoleDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody]RoleDto roleToUpdate)
        {
            _logger.LogDebug("call put method");

            if (roleToUpdate == null)
            {
                _logger.LogError("error invalid put request");
                return BadRequest();
            }

            roleToUpdate.Id = id;

            var roleEntity = _mapper.Map<RoleEntity>(roleToUpdate);
            await _unitOfWork.Repository<RoleEntity>().Update(roleEntity);
            await _unitOfWork.SaveChangesAsync();

            return Ok(roleToUpdate);
        }

        [HttpPut("{idR}/exercise/{idEx}")]
        [ProducesResponseType(200, Type = typeof(RoleDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddExercise(int idR, int idEx)
        {
            var roleEntity = await _unitOfWork.Repository<RoleEntity>().FindById(idR);

            var exerciseEntity = await _unitOfWork.Repository<ExerciseEntity>().FindById(idEx);

            RoleExerciseEntity roleExerciseEntity = new RoleExerciseEntity { Role = roleEntity, Exercise = exerciseEntity };

            await _unitOfWork.Repository<RoleExerciseEntity>().Insert(roleExerciseEntity);
            await _unitOfWork.SaveChangesAsync();

            var roleUpdated = _mapper.Map<RoleDto>(roleEntity);

            return Ok(roleUpdated);
        }

        [HttpDelete("{idR}/exercise/{idEx}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteExercise(int idR, int idEx)
        {

            var roleEntity = await _unitOfWork.Repository<RoleEntity>().FindById(idR);

            var exerciseEntity = await _unitOfWork.Repository<ExerciseEntity>().FindById(idEx);

            RoleExerciseEntity roleExerciseEntity = new RoleExerciseEntity { Role = roleEntity, Exercise = exerciseEntity };
            var done = await _unitOfWork.Repository<RoleExerciseEntity>().Delete(roleExerciseEntity);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(done);
            }
            return NotFound();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogDebug("call delete method");

            await _unitOfWork.Repository<RoleEntity>().Delete(id);
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }

        [HttpDelete]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Clear()
        {
            _logger.LogDebug("call clear method");

            await _unitOfWork.Repository<RoleEntity>().Clear();
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }
    }
}
