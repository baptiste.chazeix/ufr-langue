﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataLibrary;
using DataLibrary.exercise;
using DataLibrary.group;
using DataLibrary.many_to_many;
using DTOs.exercise;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace RestfulApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExerciseController : ControllerBase
    {
        private readonly ILogger<ExerciseController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ExerciseController(ILogger<ExerciseController> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<ExerciseDto>))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll([FromQuery] int index = 0, [FromQuery] int count = 10)
        {
            _logger.LogDebug("call getAll method");
            var exercises = await _unitOfWork.Repository<ExerciseEntity>().QueryHelper().Include(e => e.Questions).Get(index, count);
            var exerciseDto = _mapper.Map<IEnumerable<ExerciseDto>>(exercises);
            return Ok(exerciseDto);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(ExerciseDto))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            _logger.LogDebug("call get method");
            var exercise = await _unitOfWork.Repository<ExerciseEntity>().FindById(id);
            if (exercise != null)
            {
                var exerciseDto = _mapper.Map<ExerciseDto>(exercise);
                return Ok(exerciseDto);
            }
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(ExerciseDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]ExerciseDto exercise)
        {
            _logger.LogDebug("call post method");
            if (exercise == null)
            {
                _logger.LogError("error invalid post request");
                return BadRequest();
            }

            var exerciseEntity = _mapper.Map<ExerciseEntity>(exercise);
            var resultPost = await _unitOfWork.Repository<ExerciseEntity>().Insert(exerciseEntity);
            await _unitOfWork.SaveChangesAsync();

            var resultDto = _mapper.Map<ExerciseDto>(resultPost);
            return CreatedAtAction(nameof(Get), new { id = resultDto.Id }, resultDto);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(ExerciseDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody]ExerciseDto exerciseToUpdate)
        {
            _logger.LogDebug("call put method");

            if (exerciseToUpdate == null)
            {
                _logger.LogError("error invalid put request");
                return BadRequest();
            }

            exerciseToUpdate.Id = id;

            var exerciseEntity = _mapper.Map<ExerciseEntity>(exerciseToUpdate);
            await _unitOfWork.Repository<ExerciseEntity>().Update(exerciseEntity);
            await _unitOfWork.SaveChangesAsync();

            return Ok(exerciseToUpdate);
        }

        [HttpPut("{idEx}/group/{idG}")]
        [ProducesResponseType(200, Type = typeof(ExerciseDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddGroup(int idEx, int idG)
        {
            var exerciseEntity = await _unitOfWork.Repository<ExerciseEntity>().FindById(idEx);

            var groupEntity = await _unitOfWork.Repository<GroupEntity>().FindById(idG);

            GroupExerciseEntity groupExerciseEntity = new GroupExerciseEntity { Group = groupEntity, Exercise = exerciseEntity };

            await _unitOfWork.Repository<GroupExerciseEntity>().Insert(groupExerciseEntity);
            await _unitOfWork.SaveChangesAsync();

            var exerciseUpdated = _mapper.Map<ExerciseDto>(exerciseEntity);

            return Ok(exerciseUpdated);
        }

        [HttpDelete("{idEx}/group/{idG}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteGroup(int idEx, int idG)
        {

            var exerciseEntity = await _unitOfWork.Repository<ExerciseEntity>().FindById(idEx);

            var groupEntity = await _unitOfWork.Repository<GroupEntity>().FindById(idG);

            GroupExerciseEntity groupExerciseEntity = new GroupExerciseEntity { Group = groupEntity, Exercise = exerciseEntity };
            var done = await _unitOfWork.Repository<GroupExerciseEntity>().Delete(groupExerciseEntity);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(done);
            }
            return NotFound();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogDebug("call delete method");

            var done = await _unitOfWork.Repository<ExerciseEntity>().Delete(id);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(done);
            }
            return NotFound();
        }

        [HttpDelete]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Clear()
        {
            _logger.LogDebug("call clear method");

            await _unitOfWork.Repository<ExerciseEntity>().Clear();
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }
    }
}
