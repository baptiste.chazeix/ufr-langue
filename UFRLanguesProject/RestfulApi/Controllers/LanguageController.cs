﻿using AutoMapper;
using DataLibrary;
using DataLibrary.others;
using DTOs.others;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LanguageController : ControllerBase
    {
        private readonly ILogger<LanguageController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LanguageController(ILogger<LanguageController> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<LanguageDto>))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll([FromQuery] int index = 0, [FromQuery] int count = 10)
        {
            _logger.LogDebug("call getAll method");

            var languages = await _unitOfWork.Repository<LanguageEntity>().GetItems(index, count);
            var languageDto = _mapper.Map<IEnumerable<LanguageDto>>(languages);

            return Ok(languageDto);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(LanguageDto))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(LanguageEnum id)
        {
            _logger.LogDebug("call get method");
            var language = await _unitOfWork.Repository<LanguageEntity>().FindById(id);
            if (language != null)
            {
                var languageDto = _mapper.Map<LanguageDto>(language);
                return Ok(languageDto);
            }
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(LanguageDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]LanguageDto language)
        {
            _logger.LogDebug("call post method");

            if (language == null)
            {
                _logger.LogError("error invalid post request");
                return BadRequest();
            }

            var languageEntity = _mapper.Map<LanguageEntity>(language);
            await _unitOfWork.Repository<LanguageEntity>().Insert(languageEntity);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = language.Type }, language);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(LanguageDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(LanguageEnum type, [FromBody]LanguageDto languageToUpdate)
        {
            _logger.LogDebug("call put method");

            if (languageToUpdate == null)
            {
                _logger.LogError("error invalid put request");
                return BadRequest();
            }

            languageToUpdate.Type = type;

            var languageEntity = _mapper.Map<LanguageEntity>(languageToUpdate);
            await _unitOfWork.Repository<LanguageEntity>().Update(languageEntity);
            await _unitOfWork.SaveChangesAsync();

            return Ok(languageToUpdate);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete(LanguageEnum type)
        {
            _logger.LogDebug("call delete method");

            await _unitOfWork.Repository<LanguageEntity>().Delete(type);
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }

        [HttpDelete]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Clear()
        {
            _logger.LogDebug("call clear method");

            await _unitOfWork.Repository<LanguageEntity>().Clear();
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }
    }
}
