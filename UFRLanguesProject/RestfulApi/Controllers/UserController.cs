﻿using AutoMapper;
using DataLibrary;
using DataLibrary.exercise;
using DataLibrary.group;
using DataLibrary.many_to_many;
using DataLibrary.user;
using DTOs.user;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserController(ILogger<UserController> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<UserDto>))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll([FromQuery] int index = 0, [FromQuery] int count = 10)
        {
            _logger.LogDebug("call getAll method");

            var users = await _unitOfWork.Repository<UserEntity>().GetItems(index, count);
            var userDto = _mapper.Map<IEnumerable<UserDto>>(users);

            return Ok(userDto);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(UserDto))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            _logger.LogDebug("call get method");
            var user = await _unitOfWork.Repository<UserEntity>().FindById(id);
            if (user != null)
            {
                var userDto = _mapper.Map<UserDto>(user);
                return Ok(userDto);
            }
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(UserDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]UserDto user)
        {
            _logger.LogDebug("call post method");

            if (user == null)
            {
                _logger.LogError("error invalid post request");
                return BadRequest();
            }

            var userEntity = _mapper.Map<UserEntity>(user);

            await _unitOfWork.Repository<UserEntity>().Insert(userEntity);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = user.Id }, user);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(UserDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody]UserDto userToUpdate)
        {
            _logger.LogDebug("call put method");

            if (userToUpdate == null)
            {
                _logger.LogError("error invalid put request");
                return BadRequest();
            }

            userToUpdate.Id = id;

            var userEntity = _mapper.Map<UserEntity>(userToUpdate);

            await _unitOfWork.Repository<UserEntity>().Update(userEntity);
            await _unitOfWork.SaveChangesAsync();

            return Ok(userToUpdate);
        }

        [HttpPut("{idU}/group/{idG}")]
        [ProducesResponseType(200, Type = typeof(UserDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddGroup(int idU, int idG)
        {
            var groupEntity = await _unitOfWork.Repository<GroupEntity>().FindById(idG);

            var userEntity = await _unitOfWork.Repository<UserEntity>().FindById(idU);

            UserGroupEntity userGroupEntity = new UserGroupEntity { User = userEntity, Group = groupEntity };

            await _unitOfWork.Repository<UserGroupEntity>().Insert(userGroupEntity);
            await _unitOfWork.SaveChangesAsync();

            var userUpdated = _mapper.Map<UserDto>(userEntity);

            return Ok(userUpdated);
        }

        [HttpDelete("{idU}/group/{idG}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteGroup(int idU, int idG)
        {

            var groupEntity = await _unitOfWork.Repository<GroupEntity>().FindById(idG);

            var userEntity = await _unitOfWork.Repository<UserEntity>().FindById(idU);

            UserGroupEntity userGroupEntity = new UserGroupEntity { User = userEntity, Group = groupEntity };
            var done = await _unitOfWork.Repository<UserGroupEntity>().Delete(userGroupEntity);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(done);
            }
            return NotFound();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogDebug("call delete method");

            var done = await _unitOfWork.Repository<UserEntity>().Delete(id);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(done);
            }
            return NotFound();
        }

        [HttpDelete]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Clear()
        {
            _logger.LogDebug("call clear method");

            await _unitOfWork.Repository<UserEntity>().Clear();
            await _unitOfWork.SaveChangesAsync();
            
            return Ok(true);
        }
    }
}
