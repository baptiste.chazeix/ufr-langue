﻿using AutoMapper;
using DataLibrary;
using DataLibrary.exercise;
using DTOs.exercise;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class QuestionController : ControllerBase
    {
        private readonly ILogger<QuestionController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public QuestionController(ILogger<QuestionController> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<QuestionDto>))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll([FromQuery] int index = 0, [FromQuery] int count = 10)
        {
            _logger.LogDebug("call getAll method");
            var questions = await _unitOfWork.Repository<QuestionEntity>().QueryHelper().Include(q => q.Corrections).Get(index, count);
            var questionDto = _mapper.Map<IEnumerable<QuestionDto>>(questions);
            return Ok(questionDto);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(QuestionDto))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            _logger.LogDebug("call get method");
            var question = await _unitOfWork.Repository<QuestionEntity>().FindById(id);
            if (question != null)
            {
                var questionDto = _mapper.Map<QuestionDto>(question);
                return Ok(questionDto);
            }
            return NoContent();
        }

        [HttpPost("{idEx}")]
        [ProducesResponseType(201, Type = typeof(QuestionDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post(int idEx, [FromBody]QuestionDto question)
        {
            _logger.LogDebug("call post method");
            if (question == null)
            {
                _logger.LogError("error invalid post request");
                return BadRequest();
            }

            var questionEntity = _mapper.Map<QuestionEntity>(question);
            questionEntity.Exercise = await _unitOfWork.Repository<ExerciseEntity>().FindById(idEx);
            var resultPost = await _unitOfWork.Repository<QuestionEntity>().Insert(questionEntity);
            await _unitOfWork.SaveChangesAsync();

            var resultDto = _mapper.Map<QuestionDto>(resultPost);
            return CreatedAtAction(nameof(Get), new { id = resultDto.Id }, resultDto);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(QuestionDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody]QuestionDto questionToUpdate)
        {
            _logger.LogDebug("call put method");

            if (questionToUpdate == null)
            {
                _logger.LogError("error invalid put request");
                return BadRequest();
            }

            questionToUpdate.Id = id;

            var questionEntity = _mapper.Map<QuestionEntity>(questionToUpdate);
            await _unitOfWork.Repository<QuestionEntity>().Update(questionEntity);
            await _unitOfWork.SaveChangesAsync();

            return Ok(questionToUpdate);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogDebug("call delete method");

            var done = await _unitOfWork.Repository<QuestionEntity>().Delete(id);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(done);
            }
            return NotFound();
        }

        [HttpDelete]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Clear()
        {
            _logger.LogDebug("call clear method");

            await _unitOfWork.Repository<QuestionEntity>().Clear();
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }
    }
}
