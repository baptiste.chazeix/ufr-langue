﻿using AutoMapper;
using DataLibrary;
using DataLibrary.group;
using DTOs.group;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupController : ControllerBase
    {
        private readonly ILogger<GroupController> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GroupController(ILogger<GroupController> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<GroupDto>))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll([FromQuery] int index = 0, [FromQuery] int count = 10)
        {
            _logger.LogDebug("call getAll method");

            var groupes = await _unitOfWork.Repository<GroupEntity>().GetItems(index, count);
            var groupDtos = _mapper.Map<IEnumerable<GroupDto>>(groupes);
            
            return Ok(groupDtos);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(GroupDto))]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            _logger.LogDebug("call get method");
            var group = await _unitOfWork.Repository<GroupEntity>().FindById(id);
            if (group != null)
            {
                var groupDto = _mapper.Map<GroupDto>(group);
                return Ok(groupDto);
            }
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(GroupDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]GroupDto group)
        {
            _logger.LogDebug("call post method");

            if (group == null)
            {
                _logger.LogError("error invalid post request");
                return BadRequest();
            }

            var groupEntity = _mapper.Map<GroupEntity>(group);
            var overGroupId = groupEntity.OverGroup?.Id;
            if(overGroupId != null)
            {
                var overGroupEntity = await _unitOfWork.Repository<GroupEntity>().FindById(overGroupId);
                groupEntity.OverGroup = overGroupEntity;
            }

            await _unitOfWork.Repository<GroupEntity>().Insert(groupEntity);
            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = group.Id }, group);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(GroupDto))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody]GroupDto groupToUpdate)
        {
            _logger.LogDebug("call put method");

            if (groupToUpdate == null)
            {
                _logger.LogError("error invalid put request");
                return BadRequest();
            }

            groupToUpdate.Id = id;

            var groupEntity = _mapper.Map<GroupEntity>(groupToUpdate);
            await _unitOfWork.Repository<GroupEntity>().Update(groupEntity);
            await _unitOfWork.SaveChangesAsync();

            return Ok(groupToUpdate);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogDebug("call delete method");

            var done = await _unitOfWork.Repository<GroupEntity>().Delete(id);
            await _unitOfWork.SaveChangesAsync();

            if (done)
            {
                return Ok(true);
            }
            return NotFound();
        }

        [HttpDelete]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Clear()
        {
            _logger.LogDebug("call clear method");

            await _unitOfWork.Repository<GroupEntity>().Clear();
            await _unitOfWork.SaveChangesAsync();

            return Ok(true);
        }
    }
}
