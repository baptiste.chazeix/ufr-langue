﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLibrary.exercise;
using DataLibrary.group;
using DataLibrary.many_to_many;
using DataLibrary.others;
using DataLibrary.user;
using Microsoft.EntityFrameworkCore;
using Shared.share_model_api;

namespace DataLibrary
{
    public class TableStub : UfrTableContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            CreateOtherModel(modelBuilder);

            CreateUserModel(modelBuilder);

            CreateExerciseModel(modelBuilder);

            CreateGroupModel(modelBuilder);

            CreateManyToManyRelations(modelBuilder);
        }

        private void CreateUserModel(ModelBuilder modelBuilder)
        {
            UserEntity user1 = new UserEntity { Id = 1, FirstName = "Marc", LastName = "Dupont", BirthDate = new DateTime(2000, 3, 12) };
            UserEntity user2 = new UserEntity { Id = 2, FirstName = "Cyril", LastName = "Moreau", BirthDate = new DateTime(1998, 1, 26) };

            modelBuilder.Entity<UserEntity>().HasData(user1, user2);
            modelBuilder.Entity<RoleEntity>().HasData(
                new { Id = 1, Type = RoleEnum.STUDENT, UserId = 1 },
                new { Id = 2, Type = RoleEnum.TEACHER, UserId = 2 }
            );
        }

        private void CreateExerciseModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExerciseEntity>().HasData(
                new { Id = 1, LanguageId = LanguageEnum.GREC },
                new { Id = 2, LanguageId = LanguageEnum.LATIN }
            );

            modelBuilder.Entity<CategoryEntity>().HasData(
                new { Id = 1, Name = "Grammaire_Grec", Description = "Exercices de grammaire en langage grec" },
                new { Id = 2, Name = "Grammaire_Latin", Description = "Exercices de grammaire en langage latin" },
                new { Id = 3, Name = "Conjugaison_Grec", Description = "Exercices de conjugaison en langage grec" }
            );

            modelBuilder.Entity<QuestionEntity>().HasData(
                new { Id = 1, Sentence = "Ceci est la premiere question", ExerciseId = 1 },
                new { Id = 2, Sentence = "Ceci est la seconde question", ExerciseId = 2 },
                new { Id = 3, Sentence = "Ceci est une autre question", ExerciseId = 2 }
            );

            modelBuilder.Entity<CorrectionEntity>().HasData(
                new { Id = 1, Index = 1, Answer = "est", QuestionId = 1 },
                new { Id = 2, Index = 3, Answer = "premiere", QuestionId = 1 },
                new { Id = 3, Index = 3, Answer = "seconde", QuestionId = 2 },
                new { Id = 4, Index = 3, Answer = "autre", QuestionId = 3 }
            );
        }

        private void CreateGroupModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GroupEntity>().HasData(
                new { Id = 1, GroupName = "MasterGroup" },
                new { Id = 2, GroupName = "MasterGroup_G1", OverGroupId = 1 },
                new { Id = 3, GroupName = "MasterGroup_G2", OverGroupId = 1 }
            );
        }

        private void CreateOtherModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LanguageEntity>().HasData(
                new { Type = LanguageEnum.GREC },
                new { Type = LanguageEnum.LATIN }
            );
        }

        private void CreateManyToManyRelations(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExerciseCategoryEntity>().HasData(
                new { ExerciseId = 1, CategoryId = 1 },
                new { ExerciseId = 1, CategoryId = 3 },
                new { ExerciseId = 2, CategoryId = 2 }
            );

            modelBuilder.Entity<UserLanguageEntity>().HasData(
                new { UserId = 1, LanguageId = LanguageEnum.GREC },
                new { UserId = 2, LanguageId = LanguageEnum.GREC },
                new { UserId = 2, LanguageId = LanguageEnum.LATIN }
            );

            modelBuilder.Entity<UserGroupEntity>().HasData(
                new { UserId = 1, GroupId = 2 },
                new { UserId = 2, GroupId = 2 }
            );

            modelBuilder.Entity<GroupExerciseEntity>().HasData(
                new { GroupId = 2, ExerciseId = 1 },
                new { GroupId = 2, ExerciseId = 2 },
                new { GroupId = 3, ExerciseId = 2 }
            );

            modelBuilder.Entity<RoleExerciseEntity>().HasData(
                new { RoleId = 1, ExerciseId = 1 },
                new { RoleId = 2, ExerciseId = 2 }
            );
        }
    }
}
