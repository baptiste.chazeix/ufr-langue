﻿using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLibrary.many_to_many;
using DataLibrary.exercise;

namespace DataLibrary.user
{
    public class RoleEntity
    {
        public int Id { get; set; }
        public RoleEnum Type { get; set; }

        public virtual UserEntity User { get; set; }

        public virtual IEnumerable<ExerciseEntity> Exercises
        {
            get
            {
                return RolesExercises.Select(re => re.Exercise);
            }
        }

        public void AddExercise(ExerciseEntity exercise)
        {
            RolesExercises.Add(new RoleExerciseEntity() { Role = this, Exercise = exercise });
        }

        public virtual ICollection<RoleExerciseEntity> RolesExercises { get; set; } = new List<RoleExerciseEntity>();
    }
}
