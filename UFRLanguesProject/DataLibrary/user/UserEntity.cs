﻿using DataLibrary.exercise;
using DataLibrary.group;
using DataLibrary.many_to_many;
using DataLibrary.others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DataLibrary.user
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public virtual ICollection<RoleEntity> Roles { get; set; } = new List<RoleEntity>();

        public virtual IEnumerable<LanguageEntity> Languages
        {
            get
            {
                return UsersLanguages.Select(ul => ul.Language);
            }
        }

        public void AddLanguage(LanguageEntity language)
        {
            UsersLanguages.Add(new UserLanguageEntity() { User = this, Language = language });
        }

        public virtual ICollection<UserLanguageEntity> UsersLanguages { get; set; } = new List<UserLanguageEntity>();


        public virtual IEnumerable<GroupEntity> Groups
        {
            get
            {
                return UsersGroups.Select(ug => ug.Group);
            }
        }

        public void AddGroup(GroupEntity group)
        {
            UsersGroups.Add(new UserGroupEntity() { User = this, Group = group });
        }

        public virtual ICollection<UserGroupEntity> UsersGroups { get; set; } = new List<UserGroupEntity>();
    }
}
