﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataLibrary.exercise;
using DataLibrary.user;
using Shared.share_model_api;
using DataLibrary.many_to_many;

namespace DataLibrary.others
{
    public class LanguageEntity
    {
        public LanguageEnum Type { get; set; }
        public virtual ICollection<ExerciseEntity> Exercises { get; set; } = new List<ExerciseEntity>();

        public virtual IEnumerable<UserEntity> Users
        {
            get
            {
                return UsersLanguages.Select(ul => ul.User);
            }
        }

        public void AddUser(UserEntity user)
        {
            UsersLanguages.Add(new UserLanguageEntity() { User = user, Language = this });
        }

        public virtual ICollection<UserLanguageEntity> UsersLanguages { get; set; } = new List<UserLanguageEntity>();
    }
}
