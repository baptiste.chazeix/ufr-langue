﻿using DataLibrary.exercise;
using DataLibrary.group;
using DataLibrary.many_to_many;
using DataLibrary.others;
using DataLibrary.user;
using Microsoft.EntityFrameworkCore;
using Shared.share_model_api;

namespace DataLibrary
{
    public class UfrTableContext : DbContext
    {
        public virtual DbSet<UserEntity> User { get; set; }
        public virtual DbSet<RoleEntity> Role { get; set; }

        public virtual DbSet<ExerciseEntity> Exercise { get; set; }
        public virtual DbSet<CategoryEntity> Category { get; set; }
        public virtual DbSet<QuestionEntity> Question { get; set; }
        public virtual DbSet<CorrectionEntity> Correction { get; set; }

        public virtual DbSet<GroupEntity> Group { get; set; }

        public virtual DbSet<LanguageEntity> Language { get; set; }

        public UfrTableContext() { }

        public UfrTableContext(DbContextOptions<UfrTableContext> optionsBuilder)
            : base(optionsBuilder) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite($"Data Source=myFirstUFRDatabase.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // GROUP

            //GroupEntity
            modelBuilder.Entity<GroupEntity>().ToTable("Group");
            //PK
            modelBuilder.Entity<GroupEntity>().HasKey(g => g.Id);
            modelBuilder.Entity<GroupEntity>().Property(g => g.Id)
                                              .ValueGeneratedOnAdd();

            // Relation One to many GroupEntity
            // Property is int nullable because a group may not have an OverGroup
            modelBuilder.Entity<GroupEntity>().Property<int?>("OverGroupId");

            modelBuilder.Entity<GroupEntity>().HasOne(g => g.OverGroup)
                                                 .WithMany(og => og.SubGroups)
                                                 .HasForeignKey("OverGroupId");

            // GROUP


            // USER

            //User entity
            modelBuilder.Entity<UserEntity>().ToTable("User");
            //Id
            modelBuilder.Entity<UserEntity>().HasKey(n => n.Id);
            modelBuilder.Entity<UserEntity>().Property(n => n.Id)
                                             .IsRequired()
                                             .ValueGeneratedOnAdd();

            //Role entity
            modelBuilder.Entity<RoleEntity>().ToTable("Role");
            //Id
            modelBuilder.Entity<RoleEntity>().HasKey(n => n.Id);
            modelBuilder.Entity<RoleEntity>().Property(n => n.Id)
                                             .IsRequired();

            // Relation One to many between UserEntity and RoleEntity
            modelBuilder.Entity<RoleEntity>().Property<int>("UserId");

            modelBuilder.Entity<RoleEntity>().HasOne(r => r.User)
                                                 .WithMany(u => u.Roles)
                                                 .HasForeignKey("UserId");

            // USER



            //EXERCISE

            modelBuilder.Entity<ExerciseEntity>().ToTable("Exercise");
            //Id
            modelBuilder.Entity<ExerciseEntity>().HasKey(n => n.Id);
            modelBuilder.Entity<ExerciseEntity>().Property(n => n.Id)
                                                 .IsRequired()
                                                 .ValueGeneratedOnAdd();

            modelBuilder.Entity<CategoryEntity>().ToTable("Category");
            //Id
            modelBuilder.Entity<CategoryEntity>().HasKey(c => c.Id);
            modelBuilder.Entity<CategoryEntity>().Property(c => c.Id)
                                                 .IsRequired()
                                                 .ValueGeneratedOnAdd();

            modelBuilder.Entity<QuestionEntity>().ToTable("Question");
            //Id
            modelBuilder.Entity<QuestionEntity>().HasKey(q => q.Id);
            modelBuilder.Entity<QuestionEntity>().Property(q => q.Id)
                                                 .IsRequired()
                                                 .ValueGeneratedOnAdd();

            modelBuilder.Entity<CorrectionEntity>().ToTable("Correction");
            //Id
            modelBuilder.Entity<CorrectionEntity>().HasKey(c => c.Id);
            modelBuilder.Entity<CorrectionEntity>().Property(c => c.Id)
                                                 .IsRequired()
                                                 .ValueGeneratedOnAdd();


            // Relation One to many between ExerciseEntity and QuestionEntity
            modelBuilder.Entity<QuestionEntity>().Property<int>("ExerciseId");

            modelBuilder.Entity<QuestionEntity>().HasOne(q => q.Exercise)
                                                 .WithMany(ex => ex.Questions)
                                                 .HasForeignKey("ExerciseId");

            // Relation One to many between QuestionEntity and CorrectionEntity
            modelBuilder.Entity<CorrectionEntity>().Property<int>("QuestionId");

            modelBuilder.Entity<CorrectionEntity>().HasOne(c => c.Question)
                                                   .WithMany(q => q.Corrections)
                                                   .HasForeignKey("QuestionId");

            // EXERCISE

            // OTHERS //

            modelBuilder.Entity<LanguageEntity>().ToTable("Language");
            //Id
            modelBuilder.Entity<LanguageEntity>().HasKey(l => l.Type);
            modelBuilder.Entity<LanguageEntity>().Property(l => l.Type)
                                                 .IsRequired();

            // Relation One to many between QuestionEntity and LanguageEntity
            modelBuilder.Entity<ExerciseEntity>().Property<LanguageEnum>("LanguageId");

            modelBuilder.Entity<ExerciseEntity>().HasOne(ex => ex.Language)
                                                   .WithMany(l => l.Exercises)
                                                   .HasForeignKey("LanguageId");

            // OTHERS //

            // MANY TO MANY RELATIONS //

            // Relation many to many between UserEntity and LanguageEntity
            modelBuilder.Entity<UserLanguageEntity>().Property<int>("UserId");
            modelBuilder.Entity<UserLanguageEntity>().Property<LanguageEnum>("LanguageId");
            modelBuilder.Entity<UserLanguageEntity>().HasKey("UserId", "LanguageId");

            //link between UserLanguage and User
            modelBuilder.Entity<UserLanguageEntity>()
                .HasOne(ul => ul.User)
                .WithMany(u => u.UsersLanguages)
                .HasForeignKey("UserId");

            //link between UserLanguage and Language
            modelBuilder.Entity<UserLanguageEntity>()
                .HasOne(ul => ul.Language)
                .WithMany(l => l.UsersLanguages)
                .HasForeignKey("LanguageId");


            // Relation many to many between ExerciseEntity and CategoryEntity
            modelBuilder.Entity<ExerciseCategoryEntity>().Property<int>("ExerciseId");
            modelBuilder.Entity<ExerciseCategoryEntity>().Property<int>("CategoryId");
            modelBuilder.Entity<ExerciseCategoryEntity>().HasKey("ExerciseId", "CategoryId");

            //link between ExerciseCategory and Exercise
            modelBuilder.Entity<ExerciseCategoryEntity>()
                .HasOne(ec => ec.Exercise)
                .WithMany(e => e.ExercisesCategories)
                .HasForeignKey("ExerciseId");

            //link between ExerciseCategory and Category
            modelBuilder.Entity<ExerciseCategoryEntity>()
                .HasOne(ec => ec.Category)
                .WithMany(c => c.ExercisesCategories)
                .HasForeignKey("CategoryId");


            // Relation many to many between UserEntity and GroupEntity
            modelBuilder.Entity<UserGroupEntity>().Property<int>("UserId");
            modelBuilder.Entity<UserGroupEntity>().Property<int>("GroupId");
            modelBuilder.Entity<UserGroupEntity>().HasKey("UserId", "GroupId");

            //link between UserGroup and User
            modelBuilder.Entity<UserGroupEntity>()
                .HasOne(ug => ug.User)
                .WithMany(u => u.UsersGroups)
                .HasForeignKey("UserId");

            //link between UserGroup and Group
            modelBuilder.Entity<UserGroupEntity>()
                .HasOne(ug => ug.Group)
                .WithMany(g => g.UsersGroups)
                .HasForeignKey("GroupId");


            // Relation many to many between GroupEntity and ExerciseEntity
            modelBuilder.Entity<GroupExerciseEntity>().Property<int>("GroupId");
            modelBuilder.Entity<GroupExerciseEntity>().Property<int>("ExerciseId");
            modelBuilder.Entity<GroupExerciseEntity>().HasKey("GroupId", "ExerciseId");

            //link between GroupExercise and Group
            modelBuilder.Entity<GroupExerciseEntity>()
                .HasOne(ge => ge.Group)
                .WithMany(g => g.GroupsExercises)
                .HasForeignKey("GroupId");

            //link between GroupExercise and Exercise
            modelBuilder.Entity<GroupExerciseEntity>()
                .HasOne(ge => ge.Exercise)
                .WithMany(e => e.GroupsExercises)
                .HasForeignKey("ExerciseId");


            // Relation many to many between RoleEntity and ExerciseEntity
            modelBuilder.Entity<RoleExerciseEntity>().Property<int>("RoleId");
            modelBuilder.Entity<RoleExerciseEntity>().Property<int>("ExerciseId");
            modelBuilder.Entity<RoleExerciseEntity>().HasKey("RoleId", "ExerciseId");

            //link between RoleExercise and Role
            modelBuilder.Entity<RoleExerciseEntity>()
                .HasOne(re => re.Role)
                .WithMany(r => r.RolesExercises)
                .HasForeignKey("RoleId");

            //link between RoleExercise and Exercise
            modelBuilder.Entity<RoleExerciseEntity>()
                .HasOne(re => re.Exercise)
                .WithMany(e => e.RolesExercises)
                .HasForeignKey("ExerciseId");


            // MANY TO MANY RELATIONS //

            base.OnModelCreating(modelBuilder);
        }

    }
}
