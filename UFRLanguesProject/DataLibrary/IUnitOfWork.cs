﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataLibrary
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<TEntity> Repository<TEntity>() where TEntity : class; 
        
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

        Task RejectChangesAsync();
    }
}
