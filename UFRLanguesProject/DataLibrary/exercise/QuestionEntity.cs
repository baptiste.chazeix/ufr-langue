﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.exercise
{
    public class QuestionEntity
    {
        public int Id { get; set; }
        public string Sentence { get; set; }
        public virtual ExerciseEntity Exercise { get; set; }
        public virtual ICollection<CorrectionEntity> Corrections { get; set; } = new List<CorrectionEntity>();
    }
}
