﻿using DataLibrary.many_to_many;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLibrary.exercise
{
    public class CategoryEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public virtual IEnumerable<ExerciseEntity> Exercises
        {
            get
            {
                return ExercisesCategories.Select(ec => ec.Exercise);
            }
        }

        public void AddExercise(ExerciseEntity exercise)
        {
            ExercisesCategories.Add(new ExerciseCategoryEntity() { Exercise = exercise, Category = this });
        }

        public virtual ICollection<ExerciseCategoryEntity> ExercisesCategories { get; set; } = new List<ExerciseCategoryEntity>();
    }
}
