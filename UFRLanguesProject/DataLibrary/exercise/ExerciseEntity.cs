﻿using DataLibrary.group;
using DataLibrary.many_to_many;
using DataLibrary.others;
using DataLibrary.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLibrary.exercise
{
    public class ExerciseEntity
    {
        public int Id { get; set; }

        public String Name { get; set; }

        public virtual LanguageEntity Language { get; set; }

        // An exercise contains many questions
        public virtual ICollection<QuestionEntity> Questions { get; set; } = new List<QuestionEntity>();

        public virtual IEnumerable<CategoryEntity> Categories
        {
            get
            {
                return ExercisesCategories.Select(ec => ec.Category);
            }
        }

        public void AddCategory(CategoryEntity category)
        {
            ExercisesCategories.Add(new ExerciseCategoryEntity() { Exercise = this, Category = category });
        }

        public virtual ICollection<ExerciseCategoryEntity> ExercisesCategories { get; set; } = new List<ExerciseCategoryEntity>();


        public virtual IEnumerable<GroupEntity> Groups
        {
            get
            {
                return GroupsExercises.Select(ge => ge.Group);
            }
        }

        public void AddGroup(GroupEntity group)
        {
            GroupsExercises.Add(new GroupExerciseEntity() { Group = group, Exercise = this });
        }

        public virtual ICollection<GroupExerciseEntity> GroupsExercises { get; set; } = new List<GroupExerciseEntity>();


        public virtual IEnumerable<RoleEntity> Roles
        {
            get
            {
                return RolesExercises.Select(re => re.Role);
            }
        }

        public void AddRole(RoleEntity role)
        {
            RolesExercises.Add(new RoleExerciseEntity() { Role = role, Exercise = this });
        }

        public virtual ICollection<RoleExerciseEntity> RolesExercises { get; set; } = new List<RoleExerciseEntity>();
    }
}
