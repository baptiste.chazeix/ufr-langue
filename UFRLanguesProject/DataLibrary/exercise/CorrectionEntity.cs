﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.exercise
{
    public class CorrectionEntity
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string Answer { get; set; }
        public virtual QuestionEntity Question { get; set; }
    }
}
