﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary
{
    public class FluentRepository<TEntity> where TEntity : class
    {
        private readonly List<Expression<Func<TEntity, object>>> includes = new List<Expression<Func<TEntity, object>>>();
        private readonly IGenericRepository<TEntity> repository;

        public FluentRepository(IGenericRepository<TEntity> repository)
        {
            this.repository = repository;
        }
        public FluentRepository<TEntity> Include(Expression<Func<TEntity, object>> expression)
        {
            includes.Add(expression);
            return this;
        }

        public async Task<IEnumerable<TEntity>> Get(int index, int count)
        {
            return await repository.Get(index, count, includes);
        }


    }
}
