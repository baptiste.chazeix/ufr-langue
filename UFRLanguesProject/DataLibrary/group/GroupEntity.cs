﻿using DataLibrary.many_to_many;
using DataLibrary.user;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataLibrary.exercise;

namespace DataLibrary.group
{
    public class GroupEntity
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public virtual GroupEntity OverGroup { get; set; }
        public virtual ICollection<GroupEntity> SubGroups { get; set; } = new List<GroupEntity>();

        public virtual IEnumerable<UserEntity> Users
        {
            get
            {
                return UsersGroups.Select(ug => ug.User);
            }
        }

        public void AddUser(UserEntity user)
        {
            UsersGroups.Add(new UserGroupEntity() { User = user, Group = this });
        }

        public virtual ICollection<UserGroupEntity> UsersGroups { get; set; } = new List<UserGroupEntity>();


        public virtual IEnumerable<ExerciseEntity> Exercises
        {
            get
            {
                return GroupsExercises.Select(ge => ge.Exercise);
            }
        }

        public void AddExercise(ExerciseEntity exercise)
        {
            GroupsExercises.Add(new GroupExerciseEntity() { Group = this, Exercise = exercise });
        }

        public virtual ICollection<GroupExerciseEntity> GroupsExercises { get; set; } = new List<GroupExerciseEntity>();
    }
}
