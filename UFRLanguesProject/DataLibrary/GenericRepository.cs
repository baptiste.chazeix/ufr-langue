﻿using Microsoft.EntityFrameworkCore;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataLibrary
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _dbContext;
        protected readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext?.Set<TEntity>();
        }
        public IQueryable<TEntity> Set => _dbSet;

        public virtual FluentRepository<TEntity> QueryHelper()
        {
            var fluentRepository = new FluentRepository<TEntity>(this);
            return fluentRepository;
        }


        public virtual async Task<TEntity> FindById(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual async Task<IEnumerable<TEntity>> GetItems(int index, int count)
        {
            return await Get(index, count);
        }

        public virtual async Task<IEnumerable<TEntity>> Get(
            int index,
            int count,
            List<Expression<Func<TEntity, object>>> includes = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if(includes != null)
            {
                includes.ForEach(i => query = query.Include(i));
            }
            query = query.Skip(index).Take(count);
            return await Task.FromResult(query.AsEnumerable());
        }

        public virtual async Task<TEntity> Insert(TEntity item)
        {
            await _dbSet.AddAsync(item);
            return item;
        }

        public virtual async Task<bool> AddRange(params TEntity[] items)
        {
            await _dbSet.AddRangeAsync(items);
            return true;
        }

        public virtual async Task<TEntity> Update(TEntity entity)
        {
            await Task.Run(()=>_dbSet.Update(entity));
            return entity;
        }

        public virtual async Task<TEntity> Update(object id, TEntity item)
        {
            return await Update(item);
        }

        public virtual async Task<bool> Delete(TEntity entity)
        {
            return await Task.Run(() => {
                if(entity == null) { return false; }
                _dbSet.Remove(entity);
                return true;
            });
        }

        public virtual async Task<bool> Delete(object id)
        {
            var entity = await FindById(id);
            return await Delete(entity);
        }

        public virtual async Task Clear()
        {
            var allEntities = _dbSet.AsEnumerable();
            await Task.Run(() => _dbSet.RemoveRange(allEntities));
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }

        public virtual async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext?.SaveChangesAsync(cancellationToken);
        }
    }
}
