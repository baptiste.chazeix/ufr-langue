﻿using DataLibrary.exercise;
using DataLibrary.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.many_to_many
{
    public class RoleExerciseEntity
    {
        public RoleEntity Role { get; set; }
        public ExerciseEntity Exercise { get; set; }
    }
}
