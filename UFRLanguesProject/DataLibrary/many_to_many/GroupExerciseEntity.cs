﻿using DataLibrary.exercise;
using DataLibrary.group;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.many_to_many
{
    public class GroupExerciseEntity
    {
        public GroupEntity Group { get; set; }
        public ExerciseEntity Exercise { get; set; }
    }
}
