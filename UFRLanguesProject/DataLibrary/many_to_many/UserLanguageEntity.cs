﻿using DataLibrary.others;
using DataLibrary.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.many_to_many
{
    public class UserLanguageEntity
    {
        public UserEntity User { get; set; }
        public LanguageEntity Language { get; set; }
    }
}
