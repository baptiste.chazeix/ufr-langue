﻿using DataLibrary.exercise;

namespace DataLibrary.many_to_many
{
    public class ExerciseCategoryEntity
    {
        public ExerciseEntity Exercise { get; set; }
        public CategoryEntity Category { get; set; }
    }
}