﻿using DataLibrary.group;
using DataLibrary.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary.many_to_many
{
    public class UserGroupEntity
    {
        public UserEntity User { get; set; }
        public GroupEntity Group { get; set; }
    }
}
