﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataLibrary
{
    public interface IGenericRepository<TEntity> : IDisposable, IDataManager<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Set { get; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

       FluentRepository<TEntity> QueryHelper();

        Task<TEntity> Update(TEntity entity);

        Task<bool> Delete(TEntity entity);

        Task<IEnumerable<TEntity>> Get(
            int index,
            int count,
            List<Expression<Func<TEntity, object>>> includes = null);

    }
}
