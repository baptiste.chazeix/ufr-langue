using AutoFixture;
using AutoMapper;
using DataLibrary;
using DataLibrary.exercise;
using DTOs.exercise;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestfulApi.Controllers;
using RestfulApi.Profile;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Tests
{
    [TestClass]
    public class ExerciseControllerTest
    {
     
        public static ExerciseController ExerciseController { get; set; }
        public static Fixture Fixture { get; set; } = new Fixture();
        public static DbContext DbContext { get; set; }
        public static UnitOfWork UnitOfWork { get; set; }
        public static IMapper Mapper { get; set; }

        [ClassInitialize]
        public static async Task InitTestClass(TestContext testContext)
        {
            InitController();
        }

        private static async void InitController()
        {
            var logger = new NullLogger<ExerciseController>();
            DbContext = new TestDbContext();
            DbContext.Database.EnsureCreated();
            DbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            UnitOfWork = new UnitOfWork(DbContext);
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ExerciseProfile());
            });
            Mapper = mapperConfiguration.CreateMapper();
            ExerciseController = new ExerciseController(logger, UnitOfWork, Mapper);
        }

        [TestInitialize]
        public async Task InitTests()
        {
            InitController();
        }

        [TestMethod]
        public async Task TestGetOneExercise()
        {
            //Arrange

            //Act
            var result = await ExerciseController.Get(1);
            var okResult = result as OkObjectResult;

            //Assert
            okResult.Should().NotBeNull();
            var exercise = okResult.Value as ExerciseDto;
            exercise.Id.Should().Be(1);
        }

        [TestMethod]
        public async Task TestGetAllExercises()
        {
            //Arrange

            //Act
            var result = await ExerciseController.GetAll();
            var okResult = result as OkObjectResult;

            //Assert
            okResult.Should().NotBeNull();
            var exercises = okResult.Value as IEnumerable<ExerciseDto>;
            exercises.Should().HaveCount(2);
        }

        [TestMethod]
        public async Task TestPostExercise()
        {
            //Arrange
            var newExercise = new ExerciseDto(){ Id = 3 };
            newExercise.Questions.Add(new QuestionDto { Id = 4, Sentence = "Ceci est une nouvelle question" });
            newExercise.Questions[0].Corrections.Add(new CorrectionDto { Id = 5, Index = 3, Answer = "nouvelle" });

            //Act
            var result = await ExerciseController.Post(newExercise);
            var createResult = result as CreatedAtActionResult;

            //Assert
            createResult.Should().NotBeNull();
            var createdExercise = createResult.Value as ExerciseDto;
            createdExercise.Should().NotBeNull();
            createdExercise.Id.Should().Be(3);
            createdExercise.Questions.ElementAt(0).Sentence.Should().Be("Ceci est une nouvelle question");
        }

        [TestMethod]
        public async Task TestPutExercise()
        {
            //Arrange
            var exerciseToUpdateEntity = await UnitOfWork.Repository<ExerciseEntity>().FindById(1);
            var exerciseToUpdate = Mapper.Map<ExerciseDto>(exerciseToUpdateEntity);

            //Act
            var result = await ExerciseController.Put(1,exerciseToUpdate);
            var updateResult = result as OkObjectResult;

            //Assert
            updateResult.Should().NotBeNull();
            var updatedExercise = updateResult.Value as ExerciseDto;
            updatedExercise.Should().NotBeNull();
            updatedExercise.Id.Should().Be(1);
        }

        [TestMethod]
        public async Task TestDeleteOneExercise()
        {
            //Arrange

            //Act
            var result = await ExerciseController.Delete(1);
            var deleteResult = result as OkObjectResult;

            //Assert
            deleteResult.Should().NotBeNull();
            var wasExerciseDeleted = deleteResult.Value is bool ? (bool)deleteResult.Value : false;
            wasExerciseDeleted.Should().BeTrue();
        }

        [TestMethod]
        public async Task TestClearAllExercises()
        {
            //Arrange

            //Act
            var result = await ExerciseController.Clear();
            var clearResult = result as OkObjectResult;

            //Assert
            clearResult.Should().NotBeNull();
            var wasExercisesCleared = clearResult.Value is bool ? (bool)clearResult.Value : false;
            wasExercisesCleared.Should().BeTrue();
        }

        [TestCleanup]
        public void CleanUp()
        {
            DbContext.Dispose();
        }
       
    }
}
