﻿using AutoFixture;
using AutoMapper;
using DataLibrary;
using DataLibrary.user;
using DTOs.user;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestfulApi.Controllers;
using RestfulApi.Profile;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Tests
{
    [TestClass]
    public class UserControllerTest
    {
        public static UserController UserController { get; set; }
        public static Fixture Fixture { get; set; } = new Fixture();
        public static DbContext DbContext { get; set; }
        public static UnitOfWork UnitOfWork { get; set; }
        public static IMapper Mapper { get; set; }

        [ClassInitialize]
        public static async Task InitTestClass(TestContext testContext)
        {
            InitController();
        }

        private static async void InitController()
        {
            var logger = new NullLogger<UserController>();
            DbContext = new TestDbContext();
            DbContext.Database.EnsureCreated();
            DbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            UnitOfWork = new UnitOfWork(DbContext);
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            });
            Mapper = mapperConfiguration.CreateMapper();
            UserController = new UserController(logger, UnitOfWork, Mapper);
        }

        [TestInitialize]
        public async Task InitTests()
        {
            InitController();
        }

        [TestMethod]
        public async Task TestGetOneUser()
        {
            //Arrange

            //Act
            var result = await UserController.Get(1);
            var okResult = result as OkObjectResult;

            //Assert
            okResult.Should().NotBeNull();
            var user = okResult.Value as UserDto;
            user.Id.Should().Be(1);
            user.FirstName.Should().Be("Marc");
            user.LastName.Should().Be("Dupont");
            user.BirthDate.Month.Should().Be(3);
        }

        [TestMethod]
        public async Task TestGetAllUsers()
        {
            //Arrange

            //Act
            var result = await UserController.GetAll();
            var okResult = result as OkObjectResult;

            //Assert
            okResult.Should().NotBeNull();
            var users = okResult.Value as IEnumerable<UserDto>;
            users.Should().HaveCount(2);
        }

        [TestMethod]
        public async Task TestPostUser()
        {
            //Arrange
            var newUser = new UserDto() { Id = 3, FirstName = "Julien", LastName = "Marchand", BirthDate = new DateTime(1997,9,25) };
            //newUser.Roles.Add(new RoleDto() { Id = RoleEnum.STUDENT }); Didn't work => to correct 

            //Act
            var result = await UserController.Post(newUser);
            var createResult = result as CreatedAtActionResult;

            //Assert
            createResult.Should().NotBeNull();
            var createdUser = createResult.Value as UserDto;
            createdUser.Should().NotBeNull();
            createdUser.Should().Be(newUser);
        }

        [TestMethod]
        public async Task TestPutUser()
        {
            //Arrange
            var userToUpdateEntity = await UnitOfWork.Repository<UserEntity>().FindById(1);
            var userToUpdate = Mapper.Map<UserDto>(userToUpdateEntity);
            userToUpdate.FirstName = "Marcel";
            userToUpdate.BirthDate = new DateTime(1999,7,17);

            //Act
            var result = await UserController.Put(1, userToUpdate);
            var updateResult = result as OkObjectResult;

            //Assert
            updateResult.Should().NotBeNull();
            var updatedUser = updateResult.Value as UserDto;
            updatedUser.Should().NotBeNull();
            updatedUser.Id.Should().Be(1);
            updatedUser.FirstName.Should().Be("Marcel");
            updatedUser.LastName.Should().Be("Dupont");
            updatedUser.BirthDate.Day.Should().Be(17);
        }

        [TestMethod]
        public async Task TestDeleteOneUser()
        {
            //Arrange

            //Act
            var result = await UserController.Delete(1);
            var deleteResult = result as OkObjectResult;

            //Assert
            deleteResult.Should().NotBeNull();
            var wasUserDeleted = deleteResult.Value is bool ? (bool)deleteResult.Value : false;
            wasUserDeleted.Should().BeTrue();
        }

        [TestMethod]
        public async Task TestClearAllUsers()
        {
            //Arrange

            //Act
            var result = await UserController.Clear();
            var clearResult = result as OkObjectResult;

            //Assert
            clearResult.Should().NotBeNull();
            var wasUsersCleared = clearResult.Value is bool ? (bool)clearResult.Value : false;
            wasUsersCleared.Should().BeTrue();
        }

        [TestCleanup]
        public void CleanUp()
        {
            DbContext.Dispose();
        }
    }
}
