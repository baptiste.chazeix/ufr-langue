﻿using AutoFixture;
using AutoMapper;
using DataLibrary;
using DataLibrary.group;
using DTOs.group;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestfulApi.Controllers;
using RestfulApi.Profile;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApi.Tests
{
    [TestClass]
    public class GroupControllerTest
    {
        public static GroupController GroupController { get; set; }
        public static Fixture Fixture { get; set; } = new Fixture();
        public static DbContext DbContext { get; set; }
        public static UnitOfWork UnitOfWork { get; set; }
        public static IMapper Mapper { get; set; }

        [ClassInitialize]
        public static async Task InitTestClass(TestContext testContext)
        {
            InitController();
        }

        private static async void InitController()
        {
            var logger = new NullLogger<GroupController>();
            DbContext = new TestDbContext();
            DbContext.Database.EnsureCreated();
            DbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            UnitOfWork = new UnitOfWork(DbContext);
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new GroupProfile());
            });
            Mapper = mapperConfiguration.CreateMapper();
            GroupController = new GroupController(logger, UnitOfWork, Mapper);
        }

        [TestInitialize]
        public async Task InitTests()
        {
            InitController();
        }

        [TestMethod]
        public async Task TestGetOneGroup()
        {
            //Arrange

            //Act
            var result = await GroupController.Get(1);
            var okResult = result as OkObjectResult;

            //Assert
            okResult.Should().NotBeNull();
            var group = okResult.Value as GroupDto;
            group.Id.Should().Be(1);
            group.GroupName.Should().Be("MasterGroup");
        }

        [TestMethod]
        public async Task TestGetAllGroups()
        {
            //Arrange

            //Act
            var result = await GroupController.GetAll();
            var okResult = result as OkObjectResult;

            //Assert
            okResult.Should().NotBeNull();
            var groups = okResult.Value as IEnumerable<GroupDto>;
            groups.Should().HaveCount(3);
        }

        [TestMethod]
        public async Task TestPostGroup()
        {
            //Arrange
            var newGroup = new GroupDto() { Id = 4, GroupName = "New_Group" };

            //Act
            var result = await GroupController.Post(newGroup);
            var createResult = result as CreatedAtActionResult;

            //Assert
            createResult.Should().NotBeNull();
            var createdGroup = createResult.Value as GroupDto;
            createdGroup.Should().NotBeNull();
            createdGroup.Should().Be(newGroup);
        }

        [TestMethod]
        public async Task TestPutGroup()
        {
            //Arrange
            var groupToUpdateEntity = await UnitOfWork.Repository<GroupEntity>().FindById(1);
            var groupToUpdate = Mapper.Map<GroupDto>(groupToUpdateEntity);
            groupToUpdate.GroupName = "New_name";

            //Act
            var result = await GroupController.Put(1, groupToUpdate);
            var updateResult = result as OkObjectResult;

            //Assert
            updateResult.Should().NotBeNull();
            var updatedGroup = updateResult.Value as GroupDto;
            updatedGroup.Should().NotBeNull();
            updatedGroup.Id.Should().Be(1);
            updatedGroup.GroupName.Should().Be("New_name");
        }

        [TestMethod]
        public async Task TestDeleteOneGroup()
        {
            //Arrange

            //Act
            var result = await GroupController.Delete(2);
            var deleteResult = result as OkObjectResult;

            //Assert
            deleteResult.Should().NotBeNull();
            var wasGroupDeleted = deleteResult.Value is bool ? (bool)deleteResult.Value : false;
            wasGroupDeleted.Should().BeTrue();
        }

        /* Plante en appel API alors qu'il est OK en appel natif EF avec le même stub,
         * à priori problème de contrainte de FOREIGN KEY lors de la suppression
        [TestMethod]
        public async Task TestClearAllGroups()
        {
            //Arrange

            //Act
            var result = await GroupController.Clear();
            var clearResult = result as OkObjectResult;

            //Assert
            clearResult.Should().NotBeNull();
            var wasGroupsCleared = clearResult.Value is bool ? (bool)clearResult.Value : false;
            wasGroupsCleared.Should().BeTrue();
        }
        

        [TestCleanup]
        public void CleanUp()
        {
            DbContext.Dispose();
        }
        */
    }
}
