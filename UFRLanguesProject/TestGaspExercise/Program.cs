﻿using System;
using System.Collections.Generic;
using Model.exception;
using Model.exercise;

namespace TestGaspExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choisissez un type d'exercice :");
            Console.WriteLine("1: Texte à trous");
            Console.WriteLine("2: Fonction grammaticale des mots");

            int exerciseToLaunch = Convert.ToInt32(Console.ReadLine());
            switch (exerciseToLaunch)
            {
                case 1: GaspExercise(); break;
                case 2: ColouringExercise(); break;
                default: break;
            } 
        }

        static public void GaspExercise()
        {
            var gaspText = new GaspText { Sentence = "zero un deux trois quatre cinq six" };
            gaspText.AddIndex(1);
            gaspText.AddIndex(0);
            gaspText.AddIndex(5);
            gaspText.RemoveIndex(3);
            try
            { 
                gaspText.AddIndex(9);
            }catch (IndexOutOfRangeException) { }

            while(!gaspText.IsQuestionCorrect())
            {
                Console.Clear();
                foreach (var word in gaspText.GetSentenceToComplete())
                {
                    Console.Write(word);
                    Console.Write(" ");
                }

                Console.WriteLine("\nDonner l'indice du trou à remplir : ");
                var ans1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\n Donner le mot à completer : ");
                var ans2 = Console.ReadLine();

                try
                {
                    if (gaspText.IsAnswerCorrect(ans1, ans2))
                    {
                        Console.WriteLine("Bravo !");
                    }
                    else
                    {
                        Console.WriteLine("Reponse fausse");
                    }
                }
                catch (NotIndexToAnswerException e) { Console.WriteLine(e.Message); }

                Console.WriteLine("Appuyer sur une touche pour proposer un mot. 'Q' pour quitter");
                if (Console.ReadLine() == "Q")
                {
                    break;
                }
            }

            Console.WriteLine("Correction : ");
            Dictionary<int, string> dicCorrect = gaspText.Correct();
            foreach (var corr in dicCorrect)
            {
                Console.WriteLine("mot n°" + corr.Key + " -> " + corr.Value);
            }

        }

        static public void ColouringExercise()
        {
            var colouringQuestion = new Colouring { Sentence = "this is just a test" };
            colouringQuestion.AddIndex(1, "verb");
            colouringQuestion.AddIndex(0, "subject");

            while (!colouringQuestion.IsQuestionCorrect())
            {
                Console.Clear();
                Console.WriteLine(colouringQuestion.Sentence + "\n");
                Console.WriteLine(colouringQuestion.NumberOfWordsRemaining() + " fonctions grammaticales restantes");
                Console.Write("Il reste les fonctions suivantes : ");
                foreach (string fonctionG in colouringQuestion.GetGrammaticalFunctionsToFind())
                {
                    Console.Write(fonctionG + " ");
                }

                Console.WriteLine("\n------------------------------");
                Console.WriteLine("\nDonner l'indice du mot à définir : ");
                var ans1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\n Donner la fonction grammaticale du mot : ");
                var ans2 = Console.ReadLine();

                try
                {
                    if (colouringQuestion.IsAnswerCorrect(ans1, ans2))
                    {
                        Console.WriteLine("Bravo !");
                    }
                    else
                    {
                        Console.WriteLine("Reponse fausse");
                    }
                }
                catch (NotIndexToAnswerException e) { Console.WriteLine(e.Message); }

                Console.WriteLine("Appuyer sur une touche pour définir un nouveau mot. 'Q' pour quitter");
                if (Console.ReadLine() == "Q")
                {
                    break;
                }
            }

            Console.WriteLine("Correction : ");
            Dictionary<int, string> dicCorrect = colouringQuestion.Correct();
            foreach (var corr in dicCorrect)
            {
                Console.WriteLine("mot n°" + corr.Key + " -> " + corr.Value);
            }
        }

        static public void Essaie1()
        {
            /*
            // Console.WriteLine("Hello World!");
            var gaspText = new GaspText("zero un deux trois quatre cinq six");
            //Aficher la phrase complete
            foreach (String words in gaspText.cutGaspSentence())
                Console.WriteLine(words);

            gaspText.addIndex(1);
            gaspText.addIndex(0);
            gaspText.addIndex(5);

            if (gaspText.isAnswerCorrect( 0,"zero"))
            {
                Console.WriteLine("Correct");
            }

            string[] myreps = gaspText.getAllCorrectsAnswers();

            foreach (var str in myreps)
            {
                Console.WriteLine(str);
            }

            string[] myreps2 = gaspText.getSentenceToComplete();

            foreach (var str in myreps2)
            {
                Console.WriteLine(str);
            }
            */
        }
    }
}
