using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

namespace ApiGateway
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson();

            //var authenticationProviderKey = "ProviderKey";
            //services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication(authenticationProviderKey, options =>
            //    {
            //        options.Authority = "http://localhost:5000";
            //        options.RequireHttpsMetadata = false;
            //        options.SupportedTokens = SupportedTokens.Both;
            //        options.ApiName = "ocelot";
            //    });

            services.AddOcelot();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            /*
            var authenticationUrl = "https://localhost:44343/users/isauthenticate";
            var configuration = new OcelotPipelineConfiguration
            {
                AuthenticationMiddleware = async (ctx, next) =>
                {
                    var client = new HttpClient();
                    client.DefaultRequestHeaders.Authorization = ctx.DownstreamRequest.Headers.Authorization;
                    var result = await client.GetAsync(authenticationUrl);
                    var isAuthenticate = Convert.ToBoolean(await result.Content.ReadAsStringAsync());
                    if (!isAuthenticate)
                    {
                        ctx.Errors.Add(new UnauthenticatedError("UnAuthenticate"));
                        return;
                    }
                    await next.Invoke();
                }
            };
            */
            app.UseWebSockets();
            app.UseOcelot().Wait();
            app.UseRouting();
            app.UseCors();

            //app.UseAuthentication();
            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}