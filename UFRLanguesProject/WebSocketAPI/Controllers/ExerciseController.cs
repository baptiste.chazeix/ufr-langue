﻿using DTOs.exercise;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSocketAPI.Services;

namespace WebSocketAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExerciseController : ControllerBase
    {
        private readonly IExerciseService _exerciseService;

        public ExerciseController(IExerciseService exerciseService)
        {
            _exerciseService = exerciseService;
        }

        [HttpPost("publish")]
        public async Task PublishExercise(ExerciseDto exercise)
        {
            /*var exercise = new ExerciseDto();
            exercise.Questions = new List<QuestionDto>();
            exercise.Questions.Add(new QuestionDto()
            {
                Sentence = "Une question par web socket ?"
            });*/
            await _exerciseService.PublishExercise(exercise);
        }

        [HttpGet("get")]
        public async Task GetExercise()
        {
            await _exerciseService.GetExercise("Un exercice à été récupéré.");
        }

        [HttpGet("post")]
        public async Task PostExercise()
        {
            await _exerciseService.PostExercise("Un exercice à été ajouté.");
        }

        [HttpGet("put")]
        public async Task PutExercise()
        {
            await _exerciseService.PutExercise("Un exercice à été mis à jour.");
        }

        [HttpGet("delete")]
        public async Task DeleteExercise()
        {
            await _exerciseService.DeleteExercise("Un exercice à été supprimé.");
        }
    }
}
