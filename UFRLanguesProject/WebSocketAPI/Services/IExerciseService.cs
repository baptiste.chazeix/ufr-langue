﻿using DTOs.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocketAPI.Services
{
    public interface IExerciseService
    {
        Task PublishExercise(ExerciseDto exerciseDto);
        Task GetExercise(string message);
        Task PutExercise(string message);
        Task PostExercise(string message);
        Task DeleteExercise(string message);
    }
}
