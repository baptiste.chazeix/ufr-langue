﻿using DTOs.exercise;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocketAPI.Services
{
    public class ExerciseService : Hub, IExerciseService
    {
        private readonly IHubContext<ExerciseService> _hubContext;

        public ExerciseService(IHubContext<ExerciseService> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task DeleteExercise(string message)
        {
            if (_hubContext.Clients != null)
            {
                await _hubContext.Clients.All.SendAsync("DeleteExercise", message);
            }
        }

        public async Task GetExercise(string message)
        {
            if (_hubContext.Clients != null)
            {
                await _hubContext.Clients.All.SendAsync("GetExercise", message);
            }

        }

        public async Task PublishExercise(ExerciseDto exerciseDto)
        {
            if (_hubContext.Clients != null)
            {
                await _hubContext.Clients.All.SendAsync("PublishExercise", exerciseDto);
            }
        }

        public async Task PutExercise(string message)
        {
            if (_hubContext.Clients != null)
            {
                await _hubContext.Clients.All.SendAsync("PutExercise", message);
            }
        }

        public async Task PostExercise(string message)
        {
            if (_hubContext.Clients != null)
            {
                await _hubContext.Clients.All.SendAsync("PostExercise", message);
            }
        }
    }
}
