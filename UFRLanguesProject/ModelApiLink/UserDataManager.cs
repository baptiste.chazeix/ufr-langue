﻿using AutoMapper;
using DTOs.user;
using Model.user;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ModelApiLink
{
    public class UserDataManager : IDataManager<User>
    {
        private readonly string _apiUrl = "http://localhost:22770/User";

        private readonly IMapper _mapper;

        public UserDataManager()
        {
            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<User, UserDto>().ReverseMap());
            _mapper = config.CreateMapper();
        }

        public async Task<bool> AddRange(params User[] items)
        {
            bool result = true;
            foreach (var item in items)
            {
                result &= await Insert(item) != null;
            }

            return result;
        }

        public async Task Clear()
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
            }
        }

        public async Task<bool> Delete(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
                return result.IsSuccessStatusCode;
            }
        }

        public async Task<User> FindById(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<UserDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<User>(dto);
            }
        }

        public async Task<IEnumerable<User>> GetItems(int index, int count)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/?index={index}&count={count}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<IEnumerable<UserDto>>(
                    await result.Content.ReadAsStringAsync());
                return _mapper.Map<IEnumerable<User>>(dto);
            }
        }

        public async Task<User> Insert(User item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<UserDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PostAsync($"{_apiUrl}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<UserDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<User>(resultDto);
            }
        }

        public async Task<User> Update(object id, User item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<UserDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PutAsync($"{_apiUrl}/{id}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<UserDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<User>(resultDto);
            }
        }
    }
}
