﻿using AutoMapper;
using DTOs.exercise;
using Model.exercise;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ModelApiLink
{
    public class QuestionDataManager : IDataManager<Question>
    {
        private readonly string _apiUrl = "http://localhost:22770/Question";

        private readonly IMapper _mapper;

        private readonly int _exerciseId; 

        public QuestionDataManager(int exerciseId)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<GaspText, QuestionDto>().ReverseMap();
                cfg.CreateMap<Question, QuestionDto>().ReverseMap().As<GaspText>();
            });

            _exerciseId = exerciseId;
            _mapper = config.CreateMapper();
        }

        public async Task<bool> AddRange(params Question[] items)
        {
            bool result = true;
            foreach (var item in items)
            {
                result &= await Insert(item) != null;
            }

            return result;
        }

        public async Task Clear()
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
            }
        }

        public async Task<bool> Delete(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
                return result.IsSuccessStatusCode;
            }
        }

        public async Task<Question> FindById(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<QuestionDto>(await result.Content.ReadAsStringAsync());

                return _mapper.Map<Question>(dto);
            }
        }

        public async Task<IEnumerable<Question>> GetItems(int index, int count)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/?index={index}&count={count}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<IEnumerable<QuestionDto>>(
                    await result.Content.ReadAsStringAsync());

                return _mapper.Map<IEnumerable<Question>>(dto);
            }
        }

        public async Task<Question> Insert(Question item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<QuestionDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PostAsync($"{_apiUrl}/{_exerciseId}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<QuestionDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<Question>(resultDto);
            }
        }

        public async Task<Question> Update(object id, Question item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<QuestionDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PutAsync($"{_apiUrl}/{id}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<QuestionDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<Question>(resultDto);
            }
        }
    }
}
