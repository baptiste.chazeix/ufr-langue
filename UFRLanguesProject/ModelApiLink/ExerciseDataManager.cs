﻿using AutoMapper;
using DTOs.exercise;
using Model.exercise;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ModelApiLink
{
    public class ExerciseDataManager : IDataManager<Exercise>
    {
        private readonly string _apiUrl = "http://localhost:22770/Exercise";

        private readonly IMapper _mapper;

        public ExerciseDataManager()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Exercise, ExerciseDto>().ReverseMap();
                cfg.CreateMap<GaspText, QuestionDto>().ReverseMap();
                cfg.CreateMap<Question, QuestionDto>().ReverseMap().As<GaspText>();
            });

            _mapper = config.CreateMapper();
        }

        public async Task<bool> AddRange(params Exercise[] items)
        {
            bool result = true;
            foreach (var item in items)
            {
                result &= await Insert(item) != null;
            }

            return result;
        }

        public async Task Clear()
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
            }
        }

        public async Task<bool> Delete(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
                return result.IsSuccessStatusCode;
            }
        }

        public async Task<Exercise> FindById(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<ExerciseDto>(await result.Content.ReadAsStringAsync());

                return _mapper.Map<Exercise>(dto);
            }
        }

        public async Task<IEnumerable<Exercise>> GetItems(int index, int count)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/?index={index}&count={count}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<IEnumerable<ExerciseDto>>(
                    await result.Content.ReadAsStringAsync());

                return _mapper.Map<IEnumerable<Exercise>>(dto);
            }
        }

        public async Task<Exercise> Insert(Exercise item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<ExerciseDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PostAsync($"{_apiUrl}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<ExerciseDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<Exercise>(resultDto);
            }
        }

        public async Task<Exercise> Update(object id, Exercise item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<ExerciseDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PutAsync($"{_apiUrl}/{id}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<ExerciseDto>(await result.Content.ReadAsStringAsync());
                return _mapper.Map<Exercise>(resultDto);
            }
        }
    }
}
