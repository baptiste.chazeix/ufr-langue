﻿using AutoMapper;
using DTOs.user;
using Model.user;
using Newtonsoft.Json;
using Shared;
using Shared.share_model_api;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ModelApiLink
{
    public class RoleDataManager : IDataManager<Role>
    {
        private readonly string _apiUrl = "http://localhost:22770/Role";

        private readonly IMapper _mapper;

        public RoleDataManager()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Student, RoleDto>().ReverseMap();
                cfg.CreateMap<Moderator, RoleDto>().ReverseMap();
                cfg.CreateMap<Teacher, RoleDto>().ReverseMap();
            });

            _mapper = config.CreateMapper();
        }

        private Role MapIt(RoleDto dto)
        {
            switch(dto.Type)
            {
                case RoleEnum.STUDENT: return _mapper.Map<Student>(dto);
                case RoleEnum.MODERATOR: return _mapper.Map<Moderator>(dto);
                case RoleEnum.TEACHER: return _mapper.Map<Teacher>(dto);
                default: return _mapper.Map<Student>(dto);
            }
        }

        public async Task<bool> AddRange(params Role[] items)
        {
            bool result = true;
            foreach (var item in items)
            {
                result &= await Insert(item) != null;
            }

            return result;
        }

        public async Task Clear()
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
            }
        }

        public async Task<bool> Delete(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.DeleteAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }
                return result.IsSuccessStatusCode;
            }
        }

        public async Task<Role> FindById(object id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/{id}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<RoleDto>(await result.Content.ReadAsStringAsync());

                return MapIt(dto);
            }
        }

        public async Task<IEnumerable<Role>> GetItems(int index, int count)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"{_apiUrl}/?index={index}&count={count}").ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var dto = JsonConvert.DeserializeObject<IEnumerable<RoleDto>>(
                    await result.Content.ReadAsStringAsync());

                IEnumerable<Role> items = new List<Role>();
                foreach(RoleDto role in dto)
                {
                    ((List<Role>)items).Add(MapIt(role));
                }
                return items;
            }
        }

        public async Task<Role> Insert(Role item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<RoleDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PostAsync($"{_apiUrl}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<RoleDto>(await result.Content.ReadAsStringAsync());
                return MapIt(resultDto);
            }
        }

        public async Task<Role> Update(object id, Role item)
        {
            using (var client = new HttpClient())
            {
                var dto = _mapper.Map<RoleDto>(item);

                var json = JsonConvert.SerializeObject(dto);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var result = await client.PutAsync($"{_apiUrl}/{id}", httpContent).ConfigureAwait(false);
                if (!result.IsSuccessStatusCode)
                {
                    throw new BadRequestException(result.ReasonPhrase);
                }

                var resultDto = JsonConvert.DeserializeObject<RoleDto>(await result.Content.ReadAsStringAsync());
                return MapIt(resultDto);
            }
        }
    }
}
