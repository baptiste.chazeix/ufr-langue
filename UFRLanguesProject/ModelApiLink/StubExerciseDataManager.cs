﻿using Model.exercise;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelApiLink
{
    public class StubExerciseDataManager : IDataManager<Exercise>
    {
        private List<Exercise> Exercises { get; set; }

        public StubExerciseDataManager()
        {
            Exercises = new List<Exercise>
            {
                new Exercise("Exercise 1"),
                new Exercise("Exercise 2"),
                new Exercise("Exercise 3")
            };
            Exercise exercise = new Exercise("Exercise 4 with questions")
            {
                Questions = new List<GaspText>
                {
                    new GaspText{Sentence = "Gasp Text 1" },
                    new GaspText{Sentence = "Gasp Text 2" },
                    new GaspText{Sentence = "Gasp Text 3" }
                }
                .Cast<Question>().ToList()
            };
            Exercises.Add(exercise);
        }

        public Task<bool> AddRange(params Exercise[] items)
        {
            throw new NotImplementedException();
        }

        public Task Clear()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public Task<Exercise> FindById(object id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Exercise>> GetItems(int index, int count)
        {
            return Task.Run(() =>
            {
                return Exercises.AsEnumerable();
            });
        }

        public Task<Exercise> Insert(Exercise item)
        {
            throw new NotImplementedException();
        }

        public Task<Exercise> Update(object id, Exercise item)
        {
            throw new NotImplementedException();
        }
    }
}
