﻿using DTOs.exercise;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //var connection = new HubConnectionBuilder().WithUrl("ws://localhost:50102/exerciseHub").Build(); //Lien direct à la WebSocket
            var connection = new HubConnectionBuilder().WithUrl("https://localhost:44302/connexion").Build(); //Passage par API Gateway

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

           
            
            /* Définit les actions à faire pour chaque méthode */

            connection.On<ExerciseDto>("PublishExercise", (message) =>
            {
                Console.WriteLine($"new exercise : {JsonConvert.SerializeObject(message)}");
            });
            connection.On<string>("GetExercise", (message) =>
            {
                Console.WriteLine(message);
            });
            connection.On<string>("PutExercise", (message) =>
            {
                Console.WriteLine(message);
            });
            connection.On<string>("PostExercise", (message) =>
            {
                Console.WriteLine(message);
            });
            connection.On<string>("DeleteExercise", (message) =>
            {
                Console.WriteLine(message);
            });


            await connection.StartAsync(); // Lance la connection

            var http = new HttpClient();
            HttpResponseMessage result = null;

            while (true)
            {
                string res = Console.ReadLine();

                switch (res)
                {
                    case "publish":
                        ExerciseDto dto = new ExerciseDto
                        {
                            Questions = new System.Collections.Generic.List<QuestionDto>() { new QuestionDto() { Sentence = "sentence client" } }
                        };
                        var myContent = JsonConvert.SerializeObject(dto);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        var request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = new Uri("https://localhost:44302/ws/exercise/publish"),
                            Content = byteContent
                        };
                        result = await http.SendAsync(request);
                        Console.WriteLine("Status request : " + result.StatusCode);
                        break;
                    case "get":
                        result = await http.GetAsync("https://localhost:44302/ws/exercise/get");
                        Console.WriteLine("Status request : " + result.StatusCode);
                        break;
                    case "post":
                        result = await http.GetAsync("https://localhost:44302/ws/exercise/post");
                        Console.WriteLine("Status request : " + result.StatusCode);
                        break;
                    case "put":
                        result = await http.GetAsync("https://localhost:44302/ws/exercise/put");
                        Console.WriteLine("Status request : " + result.StatusCode);
                        break;
                    case "delete":
                        result = await http.GetAsync("https://localhost:44302/ws/exercise/delete");
                        Console.WriteLine("Status request : " + result.StatusCode);
                        break;
                    default: 
                        Console.WriteLine("Sorry but I don't understand...");
                        break;
                }
            }
        }
    }
}
