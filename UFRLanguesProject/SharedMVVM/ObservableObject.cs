﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;


namespace SharedMVVM
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T member, T newValue,
                                        [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(member, newValue))
            {
                return false;
            }
            member = newValue;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected bool SetProperty<T>(T currentValue, T newValue, Action DoSet,
                                      [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(currentValue, newValue))
            {
                return false;
            }
            DoSet.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged<T>(Expression<Func<T>> expression)
        {
            string propertyName = GetPropertyName(expression);
            OnPropertyChanged(propertyName);
        }

        protected bool SetProperty<T>(ref T member, T newValue,
                                    Expression<Func<T>> expression)
        {
            if (EqualityComparer<T>.Default.Equals(member, newValue))
            {
                return false;
            }
            member = newValue;
            OnPropertyChanged(GetPropertyName(expression));
            return true;
        }

        private string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            var lambdaExpression = expression as LambdaExpression;
            MemberExpression memberExpression = lambdaExpression.Body as MemberExpression;
            return memberExpression.Member.Name;
        }
    }

}
