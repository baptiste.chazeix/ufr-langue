﻿using System;
using Model.user;
using Model.exercise;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SharedMVVM
{
    public class StudentVM : RoleVM
    {
        public StudentVM(Student model) : base(model)
        {
            Model = (Student) model;

            foreach (Exercise ex in Model.MyExercises)
            {
                Exercises.Add(new ExerciseVM(ex));
            }

        }
    }
}
