﻿using Model.user;
using Model.exercise;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Linq;

namespace SharedMVVM
{
    public class RoleVM : BaseVM<Role>
    {
        public ObservableCollection<ExerciseVM> exercises;
        public ObservableCollection<ExerciseVM> Exercises
        {
            get { return exercises; }
            set
            {
                exercises = value;
            }
        }

        public String GetRoleStr()
        {
            return Model.GetRole();
        }

        public RoleVM(Role model)
        {
            Model = model;
            Exercises = new ObservableCollection<ExerciseVM>();
/*
            foreach(Exercise ex in Model.MyExercises)
            {
                Exercises.Add(new ExerciseVM(ex));
            }
*/
            Exercises.CollectionChanged += Exercises_CollectionChange;

        }

        private void Exercises_CollectionChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    Model.MyExercises.AddRange(e.NewItems.Cast<ExerciseVM>().Select(ex => ex.Model));
                    break;
            }
        }


        // TO USE
        public void AppendExercises(ExerciseVM ex)
        {
            Model.MyExercises.Add(ex.Model);
            exercises.Add(ex);
        }

    }


}
