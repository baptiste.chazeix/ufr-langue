﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Model.user;
using Xamarin.Forms;

namespace SharedMVVM
{
    public class UserVM : BaseVM<User>
    {

        public string FirstName
        {
            get => Model.FirstName;
            set => SetProperty(Model.FirstName, value, () => Model.FirstName = value);
        }
        public string LastName
        {
            get => Model.LastName;
            set => SetProperty(Model.LastName, value, () => Model.LastName = value);
        }

        public ObservableCollection<RoleVM> roles = new ObservableCollection<RoleVM>();
        public ObservableCollection<RoleVM> Roles
        {
            get { return roles; }
            set
            {
                roles = value;
            }

        }

        public RoleVM GetBindingContext(String role)
        {
            foreach(RoleVM r in Roles)
            {
                if(r.GetRoleStr() == role)
                {
                    return r;
                }
            }
            return null;
        }

        public UserVM(User model = null)
        {
            Model = model;

            foreach (Role r in Model.Roles)
            {
                roles.Add(new RoleVM(r));
            }

            Roles.CollectionChanged += Roles_CollectionChanged;
        }

        private void Roles_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    Model.Roles.AddRange(e.NewItems.Cast<RoleVM>().Select(ex => ex.Model));
                    break;
                case NotifyCollectionChangedAction.Reset:
                    Model.Roles.Clear();
                    break;
            }
        }
    }

}
