﻿using System;
using System.Threading.Tasks;
using Model.exercise;
using Model.user;
using Xamarin.Forms;
using Model.stub;
using ModelApiLink;

using Shared;


namespace SharedMVVM
{
    public class ModeratorVM : RoleVM
    {
        public Command AddExerciseCmd { get; private set; }

        public Command GetExercisesCmd { get; private set; }

        public Command<ExerciseVM> RemoveExerciseCmd { get; private set; }

        public Command<ExerciseVM> UpdateExerciseCmd { get; private set; }

        //public ExerciseDataManager exerciseDataManager = new ExerciseDataManager();
        IDataManager<Exercise> DataManager;

        public ModeratorVM(Moderator model, IDataManager<Exercise> dataManager) : base(model)
        {
            DataManager = dataManager;

            Model = (Moderator)model;

            foreach (Exercise ex in Model.MyExercises)
            {
                Exercises.Add(new ExerciseVM(ex));
            }

            AddExerciseCmd = new Command(async () => await AddExercise());

            GetExercisesCmd = new Command(async () => await GetExercises());

            RemoveExerciseCmd = new Command<ExerciseVM>(async (ExerciseVM toDelete) => await RemoveExercise(toDelete));

            UpdateExerciseCmd = new Command<ExerciseVM>(async (ExerciseVM toUpdate) => await UpdateExercise(toUpdate));

            GetExercisesCmd.Execute(null);
        }

        
        async Task GetExercises()
        {
            var test = await DataManager.GetItems(0, 25);
            //var test = await stubDataManager.GetItems(0,1);
            Exercises.Clear();
            foreach (var item in test)
            {
                exercises.Add(new ExerciseVM(item));
                Model.AddExercises(item);
            }
        }

        async Task AddExercise()
        {
            var added = await DataManager.Insert(new Exercise { Name = "Default Exercise"});
            if(added != null)
            {
                Exercises.Add(new ExerciseVM(added));
                Model.AddExercises(added);
            }
        }

        async Task RemoveExercise(ExerciseVM toDelete)
        {
            var deleted = await DataManager.Delete(toDelete.Model.Id);
            if (deleted)
            {
                Exercises.Remove(toDelete);
                Model.MyExercises.Remove(toDelete.Model);
            }
        }

        async Task UpdateExercise(ExerciseVM toUpdate)
        {
            var updated = await DataManager.Update(toUpdate.Model.Id, toUpdate.Model);
        }
    }
}
