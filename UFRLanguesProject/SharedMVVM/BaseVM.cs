﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedMVVM
{
    public abstract class BaseVM : ObservableObject
    {
    }

    public abstract class BaseVM<TModel> : BaseVM
    {
        public TModel Model
        {
            get => model;
            set => SetProperty(ref model, value);
        }
        private TModel model;
    }

}
