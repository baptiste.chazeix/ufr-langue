﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Model.exercise;

namespace SharedMVVM
{
    public class ExerciseVM : BaseVM<Exercise>
    {
        public string Name
        {
            get => Model.Name;
            set => SetProperty(Model.Name, value, () => Model.Name = value);
        }

        public List<Question> Questions
        {
            get => Model.Questions;
            set => SetProperty(Model.Questions, value, () => Model.Questions = value);

        }

        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(_isSelected, value, () => _isSelected = value);
        }

        public ExerciseVM(Exercise model)
        {
            Model = model;
        }

    }
}
