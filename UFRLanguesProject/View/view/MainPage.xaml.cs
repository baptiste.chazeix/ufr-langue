﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.view;
using Xamarin.Forms;

namespace View.view
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            //    for (int i = Navigation.NavigationStack.Count-1; i > 1; i--)
            //    {
            //        Navigation.RemovePage(Navigation.NavigationStack[i]);
            //    }

            base.OnAppearing();
    }

        private async void Connection_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new HomePage());
        }
    }
}
