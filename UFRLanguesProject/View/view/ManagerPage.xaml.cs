﻿using System;
using View.utils;
using Xamarin.Forms;

namespace View.view
{
    public partial class ManagerPage : ContentPage
    {

        public ManagerPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            ClickableConfiguration();
        }

        private void ClickableConfiguration()
        {
            TapGestureRecognizer arrowBackClick = new TapGestureRecognizer();
            TapGestureRecognizer statisticClick = new TapGestureRecognizer();
            TapGestureRecognizer groupManagerClick = new TapGestureRecognizer();
            TapGestureRecognizer userManagerClick = new TapGestureRecognizer();
            TapGestureRecognizer profManagerClick = new TapGestureRecognizer();

            arrowBackClick.Tapped += ArrowBack_Clicked;
            statisticClick.Tapped += StatistiqueButton_Clicked;
            groupManagerClick.Tapped += GroupManagerButton_Clicked;
            userManagerClick.Tapped += UserManagerButton_Clicked;
            profManagerClick.Tapped += ProfManagerButton_Clicked;

            ArrowBack.GestureRecognizers.Add(arrowBackClick);
            statistiqueButton.GestureRecognizers.Add(statisticClick);
            groupManagerButton.GestureRecognizers.Add(groupManagerClick);
            userManagerButton.GestureRecognizers.Add(userManagerClick);
            profManagerButton.GestureRecognizers.Add(profManagerClick);
        }

        private async void ArrowBack_Clicked(object sender, EventArgs args)
        {
            Image imageButton = sender as Image;
            if (imageButton != null)
            {
                _ = ButtonSimulationUtils.ImageButtonSimulation(imageButton);
            }
            await Navigation.PopAsync();
        }

        private async void StatistiqueButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new StatistiquePage());
        }

        private async void GroupManagerButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new GroupManagerPage());
        }

        private async void UserManagerButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            //await Navigation.PushAsync();
        }

        private async void ProfManagerButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            //await Navigation.PushAsync();
        }

    }
}
