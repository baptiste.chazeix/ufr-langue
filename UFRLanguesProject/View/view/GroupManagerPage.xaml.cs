﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using View.utils;
using Xamarin.Forms;

namespace View.view
{
    public partial class GroupManagerPage : ContentPage
    {
        public ObservableCollection<string> List1 { get; set; } = new ObservableCollection<string> { "group1", "group2", "group3", "group4", "group5", "group6", "group7" };
        private int nbGroup = 7;

        public GroupManagerPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            StackPanelClickableConfiguration();

            studentList.SetBinding(ListView.ItemsSourceProperty, new Binding("List1", source: this));

            //studentList.ItemsSource = List1;
        }

        private void StackPanelClickableConfiguration()
        {
            TapGestureRecognizer arrowBackClick = new TapGestureRecognizer();
            TapGestureRecognizer statisticClick = new TapGestureRecognizer();

            arrowBackClick.Tapped += ArrowBack_Clicked;
            statisticClick.Tapped += StatistiqueButton_Clicked;

            ArrowBack.GestureRecognizers.Add(arrowBackClick);
            statistiqueButton.GestureRecognizers.Add(statisticClick);
        }

        private async void ArrowBack_Clicked(object sender, EventArgs args)
        {
            Image imageButton = sender as Image;
            if (imageButton != null)
            {
                _ = ButtonSimulationUtils.ImageButtonSimulation(imageButton);
            }
            await Navigation.PopAsync();
        }

        private void GroupCreator_Clicked(object sender, EventArgs e)
        {
            nbGroup += 1;
            List1.Add("group" + nbGroup);
        }

        private async void StatistiqueButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new StatistiquePage());
        }
    }
}
