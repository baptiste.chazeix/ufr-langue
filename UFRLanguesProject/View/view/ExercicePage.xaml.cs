﻿using System;
using System.Collections.Generic;
using Model.stub;
using Xamarin.Forms;
using View.utils;
using SharedMVVM;
using Model.user;
using Shared;
using Model.exercise;

namespace View.view
{
    public partial class ExercicePage : ContentPage
    {
        public IDataManager<Exercise> DataManager => (App.Current as App).DataManager;

        private ExerciseVM lastSelected = null;

        private ModeratorVM ViewModel => (App.Current as App).ViewModel;

        private viewmodel.TypeTri typeTri = viewmodel.TypeTri.Date;

        private List<object> list1 = new List<object>();
        private List<string> List3 = new List<string>();

        public ExercicePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            for (int i = 1; i < 10; i++)
            {
                list1.Add("catégories" + i);
                List3.Add("group" + i);
            }

            BindingContext = ViewModel;

            categorieList.ItemsSource = list1;
            groupsList.ItemsSource = List3;

            exercicesList.ItemSelected += ItemSelectedChanged;

            Button addGroup = new Button();
            addGroup.Clicked += addGroup_Clicked;
            addGroup.Text = "Nouvelle catégorie...";

            ClickableConfiguration();
        }

        private void ClickableConfiguration()
        {
            TapGestureRecognizer arrowBackClick = new TapGestureRecognizer();
            TapGestureRecognizer statisticClick = new TapGestureRecognizer();

            arrowBackClick.Tapped += ArrowBack_Clicked;
            statisticClick.Tapped += StatistiqueButton_Clicked;

            ArrowBack.GestureRecognizers.Add(arrowBackClick);
            statistiqueButton.GestureRecognizers.Add(statisticClick);
        }

        private void ItemSelectedChanged(object sender, EventArgs e) {
            if(lastSelected != null)
            {
                lastSelected.IsSelected = false;
            }
                
            ExerciseVM itemSelected = exercicesList.SelectedItem as ExerciseVM;
            lastSelected = itemSelected;
            itemSelected.IsSelected = true;
        }

        private async void ArrowBack_Clicked(object sender, EventArgs args)
        {
            Image imageButton = sender as Image;
            if (imageButton != null)
            {
                _ = ButtonSimulationUtils.ImageButtonSimulation(imageButton);
            }
            await Navigation.PopAsync();
        }

        private async void StatistiqueButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new StatistiquePage());
        }

        private void dateButton_Clicked(object sender, EventArgs e)
        {
            changeTypeTri((Button)sender, viewmodel.TypeTri.Date);
        }

        private void reussiteButton_Clicked(object sender, EventArgs e)
        {
            changeTypeTri((Button)sender, viewmodel.TypeTri.Reussite);
        }

        private void echecButton_Clicked(object sender, EventArgs e)
        {
            changeTypeTri((Button)sender, viewmodel.TypeTri.Echec);
        }

        private void changeTypeTri(Button sender, viewmodel.TypeTri choisedTypeTri)
        {
            if (typeTri != choisedTypeTri)
            {
                typeTri = choisedTypeTri;
                dateButton.BackgroundColor = (Color)Application.Current.Resources["BackgroundButtonColor"];
                reussiteButton.BackgroundColor = (Color)Application.Current.Resources["BackgroundButtonColor"];
                echecButton.BackgroundColor = (Color)Application.Current.Resources["BackgroundButtonColor"];
                sender.BackgroundColor = (Color)Application.Current.Resources["ClickedBackgroundButtonColor"];
            }
        }

        void addGroup_Clicked(System.Object sender, System.EventArgs e)
        {
            System.Console.Out.WriteLine("Passer là");
        }

        private async void RemoveExerciseClicked(object sender, EventArgs e)
        {

            var mi = ((Button)sender);
            bool answer = await DisplayAlert("Etes-vous sûr ?", "Voulez vous vraiment supprimer l'exercice ?", "Oui", "Non");
            Console.Out.WriteLine("Action: " + e);
            if (answer)
            {
                ExerciseVM ex = (ExerciseVM)exercicesList.SelectedItem;

                ViewModel.RemoveExerciseCmd.Execute(ex);
            }
        }

        private async void EditExerciseClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ExercisesManagerPage(exercicesList.SelectedItem as ExerciseVM));
        }
    }
}
