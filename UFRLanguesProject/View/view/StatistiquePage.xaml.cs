﻿using System;
using System.Collections.Generic;
using View.utils;
using Xamarin.Forms;

namespace View.view
{
    public partial class StatistiquePage : ContentPage
    {

        private string[] list1 = { "exercice1", "exercice2", "exercice3", "exercice4", "exercice5", "exercice6", "exercice7" };

        public StatistiquePage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            StackPanelClickableConfiguration();

            realizedExercice.ItemsSource = list1;
        }

        private void StackPanelClickableConfiguration()
        {
            TapGestureRecognizer backClick = new TapGestureRecognizer();
            TapGestureRecognizer loggoutClick = new TapGestureRecognizer();

            backClick.Tapped += ArrowBack_Clicked;
            loggoutClick.Tapped += LoggoutButton_Clicked;

            ArrowBack.GestureRecognizers.Add(backClick);
            LoggoutButton.GestureRecognizers.Add(loggoutClick);
        }

        private async void ArrowBack_Clicked(object sender, EventArgs args)
        {
            Image imageButton = sender as Image;
            if (imageButton != null)
            {
                _ = ButtonSimulationUtils.ImageButtonSimulation(imageButton);
            }
            await Navigation.PopAsync();
        }

        private async void LoggoutButton_Clicked(object sender, EventArgs e)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PopToRootAsync();
        }
    }
}
