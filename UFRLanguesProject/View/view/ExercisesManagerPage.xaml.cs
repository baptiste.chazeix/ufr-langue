﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using View.utils;
using SharedMVVM;

namespace View.view
{
    public partial class ExercisesManagerPage : ContentPage
    {
        private ModeratorVM ViewModel => (App.Current as App).ViewModel;

        private ExerciseVM NewExerciceVM;
        //private QuestionVM lastSelected = null;


        public ExercisesManagerPage(ExerciseVM NewExerciceVM)
        {
            this.NewExerciceVM = NewExerciceVM;

            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = NewExerciceVM;
            List<string> typeQuestion = new List<string> { "GaspQuestion", "ColoringQuestion"};

            NewQuestionTypePicker.ItemsSource = typeQuestion;

            //InitQuestionList();

            ClickableConfiguration();

            LoadExerciceDetailView();

        }
        /*
        private void InitQuestionList()
        {

            questionList.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                if (lastSelected != null)
                {
                    lastSelected.IsSelected = false;
                }

                if (e.SelectedItem != null)
                {
                    var item = e.SelectedItem as QuestionVM;
                    lastSelected = item;
                    item.IsSelected = true;

                    loadDetail(item);
                }
                else
                {
                    lastSelected = null;
                }
            };
        }

        private void loadDetail(QuestionVM item)
        {
            questionDetails.Children.Clear();

            Label sentence = new Label
            {
                Text = item.Model.ToString()
            };

            questionDetails.Children.Add(sentence);
        }
        */
        private void ClickableConfiguration()
        {
            TapGestureRecognizer arrowBackClick = new TapGestureRecognizer();
            TapGestureRecognizer statisticClick = new TapGestureRecognizer();

            arrowBackClick.Tapped += ArrowBack_Clicked;
            statisticClick.Tapped += StatistiqueButton_Clicked;
            //AddQuestionButton.Clicked += AddQuestionButton_Clicked;

            ArrowBack.GestureRecognizers.Add(arrowBackClick);
            statistiqueButton.GestureRecognizers.Add(statisticClick);
        }

        private async void ArrowBack_Clicked(object sender, EventArgs args)
        {
            Image imageButton = sender as Image;
            if (imageButton != null)
            {
                _ = ButtonSimulationUtils.ImageButtonSimulation(imageButton);
            }
            await Navigation.PopAsync();
        }

        private async void StatistiqueButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            ViewModel.UpdateExerciseCmd.Execute(NewExerciceVM);
            await Navigation.PushAsync(new StatistiquePage());
        }
        /*
        private void AddQuestionButton_Clicked(object sender, EventArgs e)
        {
            NewExerciceVM.AddQuestion(NewQuestionTypePicker.SelectedItem as string);
        }

        private void RemoveQuestionButton_Clicked(object sender, EventArgs e)
        {
            NewExerciceVM.RemoveQuestion(questionList.SelectedItem as QuestionVM);
        }

        private void UnselectItemButton_Clicked(object sender, EventArgs e)
        {
            if (lastSelected != null)
            {
                lastSelected.IsSelected = false;
            }

            questionList.SelectedItem = null;

            questionDetails.Children.Clear();

            LoadExerciceDetailView();
        }
        */
        private void LoadExerciceDetailView()
        {
            StackLayout titleStackLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal
            };

            Label exoTitleLabel = new Label
            {
                Text = "Nom de l'exercice : "
            };

            Entry exoTitleEntry = new Entry
            {
                Placeholder = "Nom",
                WidthRequest = 200,
                MinimumWidthRequest = 50
            };

            Button validateButton = new Button
            {
                Text = "Valider",
            };
            validateButton.Clicked += ValidateButton_Clicked;

            exoTitleEntry.SetBinding(
                Entry.TextProperty,
                new Binding("Name", source: NewExerciceVM, mode: BindingMode.TwoWay)
                );

            titleStackLayout.Children.Add(exoTitleLabel);
            titleStackLayout.Children.Add(exoTitleEntry);

            questionDetails.Children.Add(titleStackLayout);
            questionDetails.Children.Add(validateButton);
        }

        private void ValidateButton_Clicked(object sender, EventArgs e)
        {
            ViewModel.UpdateExerciseCmd.Execute(NewExerciceVM);
            Navigation.PopAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            ViewModel.UpdateExerciseCmd.Execute(NewExerciceVM);
            return base.OnBackButtonPressed();
        }
    }
}
