﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using View.utils;
using Xamarin.Forms;

namespace View.view
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            StackPanelClickableConfiguration();
        }

        private void StackPanelClickableConfiguration()
        {
            TapGestureRecognizer statisticClick = new TapGestureRecognizer();
            TapGestureRecognizer exerciceClick = new TapGestureRecognizer();
            TapGestureRecognizer managerClick = new TapGestureRecognizer();

            statisticClick.Tapped += StatistiqueButton_Clicked;
            exerciceClick.Tapped += ExerciceButton_Clicked;
            managerClick.Tapped += ManagerButton_Clicked;

            exerciceButton.GestureRecognizers.Add(exerciceClick);
            managerButton.GestureRecognizers.Add(managerClick);
            statistiqueButton.GestureRecognizers.Add(statisticClick);
        }

        private async void ExerciceButton_Clicked(object sender, EventArgs e)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new ExercicePage());
        }

        private async void ManagerButton_Clicked(object sender, EventArgs e)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new ManagerPage());
        }

        private async void StatistiqueButton_Clicked(object sender, EventArgs args)
        {
            StackLayout stackLayoutButton = sender as StackLayout;
            if (stackLayoutButton != null)
            {
                _ = ButtonSimulationUtils.StackLayoutButtonSimulation(stackLayoutButton);
            }
            await Navigation.PushAsync(new StatistiquePage());
        }
    }
}
