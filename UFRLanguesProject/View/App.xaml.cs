﻿using System;
using View.view;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ModelApiLink;
using Model.stub;
using Shared;
using Model.exercise;
using SharedMVVM;
using Model.user;

namespace View
{
    public partial class App : Application
    {
        public ModeratorVM ViewModel { get; set; }
        public IDataManager<Exercise> DataManager { get; set; } = new ExerciseDataManager();
        public App()
        {
            InitializeComponent();

            ViewModel = new ModeratorVM(new Moderator(), DataManager);

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
