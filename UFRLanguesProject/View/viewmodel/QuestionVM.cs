﻿using System;
using System.ComponentModel;
using Model.exercise;
namespace View.viewmodel
{
    public class QuestionVM: INotifyPropertyChanged
    {

        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IsSelected"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Question Model { get; set; }

        public QuestionVM(Question model)
        {
            Model = model;
        }

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
                Console.Out.WriteLine("Action: PropertyChanged ");
            }
        }
    }
}
