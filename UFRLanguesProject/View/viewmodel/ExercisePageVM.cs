﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Model.exercise;
using Model.stub;
using Xamarin.Forms.Internals;

namespace View.viewmodel
{
    public class ExercisePageVM
    {
        private Stub stub = new Stub();
        public List<Exercise> model = new List<Exercise>();
        private ObservableCollection<ExerciseVM> items;
        public ObservableCollection<ExerciseVM> Items
        {
            get { return items; }
            set
            {
                if (items != value)
                {
                    items = value;
                    value.ForEach(item =>
                   {
                       model.Add(item.Model);
                   });
                }
            }
        }


        public ExercisePageVM()
        {
            Items = new ObservableCollection<ExerciseVM>();
        }

        public void RemoveExercise(ExerciseVM exercise)
        {
            Items.Remove(exercise);
        }

        public void AddExercise(ExerciseVM exerciseVM)
        {
         
            Items.Add(exerciseVM);
            model.Add(exerciseVM.Model);
            
        }

    }
}
