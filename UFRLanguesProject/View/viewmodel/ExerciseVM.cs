﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Model.exercise;
using Model.stub;

namespace View.viewmodel
{
    public class ExerciseVM : INotifyPropertyChanged
    {
        public Exercise Model { get; set; }
        private Stub stub = new Stub();

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                if (Name != value)
                {
                    _Name = value;
                    Model.Name = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IsSelected"));
            }
        }

        private ObservableCollection<QuestionVM> items;
        public ObservableCollection<QuestionVM> Items
        {
            get { return items; }
            set
            {
                items = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
                Console.Out.WriteLine("Action: PropertyChanged ");
            }
        }

        public ExerciseVM() : this(new Exercise(""))
        {
        }

        public ExerciseVM(Exercise model)
        {
            Model = model;
            IsSelected = false;

            Items = new ObservableCollection<QuestionVM>();
            foreach (Question question in Model.Questions)
            {
                Items.Add(new QuestionVM(question));
            }
            
        }

        public void AddQuestion(string questionType)
        {
            Question question;
            QuestionVM questionVM;
            switch (questionType)
            {
                case "GaspQuestion":
                    question = new GaspText { Sentence = "Nouvelle question vide" };
                    questionVM = new QuestionVM(question);
                    /* En attendant la liaison model vers VM */
                    items.Add(questionVM);
                    /* ------------------------------------- */
                    Model.Questions.Add(questionVM.Model);
                    break;
                case "ColoringQuestion":
                    question = new Colouring { Sentence = "Nouvelle question vide" };
                    questionVM = new QuestionVM(question);
                    /* En attendant la liaison model vers VM */
                    items.Add(questionVM);
                    /* ------------------------------------- */
                    Model.Questions.Add(questionVM.Model);
                    break;
                default:
                    question = new GaspText { Sentence = "Nouvelle question vide" };
                    questionVM = new QuestionVM(question);
                    /* En attendant la liaison model vers VM */
                    items.Add(questionVM);
                    /* ------------------------------------- */
                    Model.Questions.Add(questionVM.Model);
                    break;
            }
        }

        public void RemoveQuestion(QuestionVM question)
        {
            /* En attendant la liaison model vers VM */
            items.Remove(question);
            /* ------------------------------------- */
            //Model.Questions.Remove(question);
        }
    }
}
