﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace View.utils
{
    public static class ButtonSimulationUtils
    {
        public static async Task StackLayoutButtonSimulation(StackLayout stackLayoutButton)
        {
            await stackLayoutButton.FadeTo(0, 250);
            _ = stackLayoutButton.FadeTo(1, 250);
        }

        public static async Task ImageButtonSimulation(Image imageButton)
        {
            await imageButton.FadeTo(0, 250);
            _ = imageButton.FadeTo(1, 250);
        }
    }
}
