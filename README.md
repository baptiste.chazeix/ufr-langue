# IMPORTANT

Le lien Model API est fait dans l'appli logicielle (View_folder -> View).
Il y a des commandes pour Get/Add/Delete/Update (voir dans SharedMVVM -> ModeratorVM).
Nous utilisons directement la ModeratorVM en VM principale de l'application pour la réutiliser partout via le App.Current.
Merci de d'abord lancer la Restful API en IIS Express, puis de lancer la View.UWP pour tester les requetes API + changements en BDD.
Nous avons rencontré des problèmes avec les appels API depuis un simulateur sur mobile, nous n'avons donc pas eu de résultats concluant pour l'appli étudiante.
La gestion des questions (ajout/suppression/modification) n'a pas pu être mis en place par manque de temps.

# UFR Langues

Le projet UFR Langues a pour but de développer une application logicielle ainsi qu'une application mobile dans l'optique de permettre aux professeurs de l'UCA
UFR Langues de créer des exercices de latin et de grec pour ses étudiants. Les étudiants auront ensuite accès à ces exercices via l'application mobile et pourront répondre
aux questions, découvrir leurs résultats, consulter la correction ... etc

Pour développer ces applications nous avons décidé de développer la partie vue en Xamarin. 
En effet nous voulons permettre aux utilisateurs d'accéder à notre application sous plusieurs OS (Windows 10, MacOS, iOS, Android, Linux).

### PS : Lancez la solution UFRLanguesProject.sln (la seconde solution est utilisée pour l'éxécution des CI sur gitlab)

# View 

La partie UI de l'application est codée à l'aide du framework Xamarin qui nous permet de développer des applications mobiles et de bureau plus simplement.<br/>
Notre vue sera divisée en 2 parties, d'un côté la vue admin qui sera utilisée par les professeurs via l'application logicielle. <br/>
De l'autre la vue étudiante sur laquelle les élèves auront accès à leur exercices depuis l'application sur leur téléphone mobile. <br/>
Pour le moment la vue n'est implémentée pour le moment que sous MacOS et iOS.

### Démarrage de la vue (sous MacOS) :

* Procédure à suivre
    * lancer à partir d'un Mac, l'application "View.Mac" (dossier View_folder)

# API Gateway 

L'API Gateway est codée en .NET Core, celle-ci nous permet de contrôler les appels API qui seront fait par le client. <br/>
L'API Gateway constitue un intermédiaire entre l'appel client et les API utilisées par l'application. <br/>
Celle-ci a pour rôle de valider ou non les appels qui seront fait à l'API en fonction de l'utilisateur qui en fera la demande. <br/>

### Démarrage de la gateway :

Démarrage simple via IIS, lancer la Gateway seule est inutile il faut la lancer avec une des 2 APIs

# API Restful

L'API Restful est codée en ASP .NET Core. <br/>
Son rôle est de fournir un point d'accès au client afin de créer, modifier, supprimer des éléments de la base, cette base de données stocke 
l'ensemble des données de l'application. <br/>
Pour donner un exemple concret, la création d'un exercice par un professeur se fait par l'appel à l'API Restful grâce à une requête HTTP POST. <br/>
L'API va ensuite déléguer l'ajout en base de données à Entity Framework afin de persister les données de l'application.

### Démarrage de la restful :

On passe par la Gateway pour requêter la Restful Api
* Procédure à suivre :
    * lancer la RestfulApi (dossier API_REST) via IIS
    * lancer l' ApiGateway (dossier ApiGateway) via IIS également
    * la restful se lance automatiquement sur swagger
    * il sera possible d'effectuer les requêtes sur l'API directement depuis l'interface de swagger (CRUD)

# WebSocket

L'API WebSocket est codée en ASP .NET Core. <br/>
Son rôle est de notifier les utilisateurs de l'application, d'un événement particulier sur l'application. <br/>
Pour donner un exemple, lorsqu'un professeur publiera un nouvel exercice via l'appel à l'API Restful, l'API de WebSocket aura pour mission de notifier
tous les étudiants concernés par cet exercice, qu'un nouvel exercice est disponible. <br/>

### Démarrage de la websocket :

On passe par la Gateway pour requêter la WebSocket Api
* Procédure à suivre :
    * lancer la WebSocketAPI (dossier WebSocket) via IIS
    * lancer l' ApiGateway (dossier ApiGateway) via IIS également
    * démarrer une nouvelle instance de l'application console Client (dossier WebSocket) pour simuler un client de l'application qui va requêter l'API WebSocket
    * vous pouvez écrire les commandes que vous désirez dans le terminal, voici la liste :
        * get -> renvoie un message à tout les clients connectés qui stipule qu'un exercice à été récupéré.
        * post -> renvoie un message à tout les clients connectés qui stipule qu'un exercice à été ajouté.
        * put -> renvoie un message à tout les clients connectés qui stipule qu'un exercice à été mis à jour.
        * delete -> renvoie un message à tout les clients connectés qui stipule qu'un exercice à été supprimé.
        * publish -> partage à tout les clients l'exercice qui est créé.
    * le terminal (représentant le client) affichera les réponses des requêtes

### PS : si vous rencontrez l'erreur ci-dessous lors du démarage de la websocket, recommencez l'opération.
    System.Net.Http.HttpRequestException: 'Aucune connexion n’a pu être établie car l’ordinateur cible l’a expressément refusée.'

# DTOs

Les DTOs sont des objets métiers représentant les données qui seront transférées <b>à</b> l'API et/ou <b>par</b> l'API pour effectuer des opérations sur la base de données de notre application. <br/>
Dans notre cas nous avons des DTOs représentant nos exercices, nos utilisateurs et nos groupes (les données que nous devons stocker en base).

# Entity Framework

Entity Framework est un framework que nous utilisons pour la gestion de notre base de données. <br/>
Le framework nous permet directement de convertir des données stockés en base, en objets métiers C# et inversement. <br/>
Pour le moment nous utilisons une base de données en local avec SQLite pour le stockage de toutes les informations.

### Démarrage de entity framework :

* Procédure à suivre :
    * il est possible de tester notre DataLibrary via l'application console "testDataLibrary" (dossier EntityFramework)
    * lancer une nouvelle instance de "testDataLibrary" pour observer les modifications faites sur la BDD locale requétée par EF

# NOTES IMPORTANTES

* Nous n'avons pas pu mettre en place toutes les relations de base de données avec EF (ayant rencontré pas mal de problèmes qui nous ont ralenti), mais le lien Restful -> EF est OK.
 
# NOTES PERSONNELLES

SonarQube :
* image : https://hub.docker.com/_/sonarqube/
* git d'exemple pour yml : https://github.com/jhipster/jhipster-dotnetcore/blob/master/generators/server/templates/dotnetcore/SonarAnalysis.ps1.ejs


